//#define msgx

#include "semval.ih"

// see ../parser/README.args for details

    // args is a SemVal argument structure
SemVal smvArgs2code(SemVal *args)
{
    SemVal *sv = (SemVal *)args->code;          // get the 1st argument

    for (unsigned idx = 0, end = args->value; idx != end; ++idx)
    {
        msg("PRE push: arg. %d has type %x", idx, svType(sv));

        svPushStack(&sv[idx]);

        msg("POST push: arg. %d has type %x", idx, svType(sv));

        msg("code length of arg %d: %d", idx, sv[idx].codelen);
    }

    return SVmove(args);
}
