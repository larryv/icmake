//#define XERR
#include "deadcode.ih"

void deadInit()
{
    dp_dead = rss_realloc(NULL, sizeof(unsigned));
    dp_dead[0] = 0;                  // initially: no dead code
}
