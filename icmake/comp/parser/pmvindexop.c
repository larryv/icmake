#include "parser.ih"

SemVal pmv_indexOp(SemVal *larg, SemVal *rarg)
{
    register int ok;
    ExprType type = f_element;

    svPushStack(larg);                      // arg to stack 
    svPushStack(rarg);                      // arg to stack 

    // This follows the code of `p_twoArgs.c' to compute a list/string    
    // element                                                          

                                            // first arg must be int 
    if (!svTestType(larg, e_int))           // first expression is no int 
        svPtrSwap(&larg, &rarg);

    if ( (ok = svTestType(larg, e_int)) )    // right arg must be int    
    {                               // second arg == list: ok 
        if (!(ok = svTestType(rarg, e_list)))
        {                           // second arg == string: ok 
            ok = svTestType(rarg, e_str);
            type = f_str_el;        // string element requested 
        }
    }

    if (ok)
    {
        svCatenate(rarg, larg);    // make one code vector 
        p_callRss(rarg, type);
    }
    else
    {
        util_semantic(sem_typeConflict, gp_funstring[type]);
        svDestroy(larg);
    }

    return SVmove(rarg);
}
