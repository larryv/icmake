#include "parser.ih"

SemVal pmv_threeArgs(ExprType type, SemVal *larg, SemVal *marg, SemVal *rarg)
{
    register int ok;

    svPushStack(larg);                             // arg to stack 
    svPushStack(marg);                             // arg to stack 
    svPushStack(rarg);                             // arg to stack 

    switch ((FunNr)type)
    {
        case f_substr:
            ok = svTestType(larg, e_str) && 
                svTestType(marg, e_int) && svTestType(rarg, e_int);
        break;

        default:
            ok = 0;
        break;
    }

    if (ok)
    {
        svCatenate(rarg, marg);        // make one code vector 
        svCatenate(rarg, larg);          
        p_callRss(rarg, type);
        return SVmove(rarg);
    }

    util_semantic(sem_typeConflict, gp_funstring[type]);
    return pmv_semVal(3, larg, marg, rarg);
}




