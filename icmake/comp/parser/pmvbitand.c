//#define msgx
#include "parser.ih"

SemVal pmv_bitand(SemVal *lval, SemVal *rval)
{
    msg("lval len = %d, rval len = %d", svCodeLength(lval), \
                                        svCodeLength(rval));

    if (p_testBinOp(op_band, lval, rval))
        return pmv_semVal(2, lval, rval);     // test for correct types 

    svBool2int(lval);                       // convert pending booleans 
    svBool2int(rval);

    msg("lval len = %d, rval len = %d", svCodeLength(lval),\
                                        svCodeLength(rval));

    if (p_conflict(lval, rval, op_band))    // test type p_conflict 
        return pmv_semVal(2, lval, rval);

    if ((svType(lval) & svType(rval) & (unsigned)~e_typeMask) == e_const)
        svSetValue(lval, svValue(lval) & svValue(rval));
    else
        *lval = pmv_binOp(lval, rval, op_band);

    msg("lval len = %d", svCodeLength(lval));

    return SVmove(lval);
}

