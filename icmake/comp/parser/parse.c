/* A Bison parser, made by GNU Bison 3.4.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2019 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Undocumented macros, especially those whose name start with YY_,
   are private implementation details.  Do not rely on them.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.4.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "grammar"

    //#define msgx

    // inc/preamble.1

    #include "parser.ih"

#line 78 "parse.c"

# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Use api.header.include to #include this header
   instead of duplicating it here.  */
#ifndef YY_YY_TOKENS_H_INCLUDED
# define YY_YY_TOKENS_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    ARG_HEAD = 258,
    ARG_TAIL = 259,
    ASCII = 260,
    BREAK = 261,
    CHDIR = 262,
    CMD_HEAD = 263,
    CMD_TAIL = 264,
    CONTINUE = 265,
    C_BASE = 266,
    C_EXT = 267,
    C_PATH = 268,
    G_BASE = 269,
    G_EXT = 270,
    G_DEXT = 271,
    G_PATH = 272,
    ELEMENT = 273,
    ELSE = 274,
    EVAL = 275,
    EXEC = 276,
    EXECUTE = 277,
    EXISTS = 278,
    EXIT = 279,
    FGETS = 280,
    FIELDS = 281,
    FOR = 282,
    FPRINTF = 283,
    GETENV = 284,
    GETCH = 285,
    GETPID = 286,
    GETS = 287,
    IDENTIFIER = 288,
    IF = 289,
    INT = 290,
    LIST = 291,
    LISTFIND = 292,
    LISTLEN = 293,
    LISTUNION = 294,
    MAKELIST = 295,
    ECHO_TOKEN = 296,
    NUMBER = 297,
    PRINTF = 298,
    PUTENV = 299,
    RETURN = 300,
    STAT = 301,
    STRCHR = 302,
    STRING = 303,
    STRINGTYPE = 304,
    STRLEN = 305,
    STRLWR = 306,
    RESIZE = 307,
    STRUPR = 308,
    STRFIND = 309,
    STRFORMAT = 310,
    SUBSTR = 311,
    SYSTEM = 312,
    TRIM = 313,
    TRIMLEFT = 314,
    TRIMRIGHT = 315,
    VOID = 316,
    WHILE = 317,
    AND_IS = 318,
    OR_IS = 319,
    XOR_IS = 320,
    SHL_IS = 321,
    SHR_IS = 322,
    DIV_IS = 323,
    MINUS_IS = 324,
    MUL_IS = 325,
    MOD_IS = 326,
    PLUS_IS = 327,
    OR = 328,
    AND = 329,
    EQUAL = 330,
    NOT_EQUAL = 331,
    SMALLER_EQUAL = 332,
    GREATER_EQUAL = 333,
    OLDER = 334,
    YOUNGER = 335,
    SHL = 336,
    SHR = 337,
    INC = 338,
    DEC = 339
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_TOKENS_H_INCLUDED  */



#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && ! defined __ICC && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  11
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   913

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  109
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  85
/* YYNRULES -- Number of rules.  */
#define YYNRULES  220
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  365

#define YYUNDEFTOK  2
#define YYMAXUTOK   339

/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                                \
  ((unsigned) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    96,     2,     2,     2,    95,    80,     2,
     106,   102,    93,    91,   103,    92,     2,    94,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    75,   107,
      83,    63,    84,    74,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,   100,     2,   105,    79,     2,   104,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   108,    78,   101,    99,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    76,    77,
      81,    82,    85,    86,    87,    88,    89,    90,    97,    98
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   118,   118,   121,   125,   132,   139,   145,   150,   156,
     156,   163,   163,   169,   169,   175,   183,   185,   188,   194,
     201,   206,   208,   212,   219,   226,   226,   235,   241,   243,
     245,   249,   254,   261,   263,   271,   271,   278,   284,   289,
     294,   299,   304,   309,   314,   319,   324,   329,   334,   339,
     344,   349,   354,   359,   364,   369,   374,   379,   384,   389,
     394,   399,   404,   409,   414,   419,   424,   429,   434,   439,
     444,   449,   454,   459,   464,   469,   474,   480,   485,   490,
     495,   500,   506,   511,   513,   519,   526,   532,   536,   538,
     540,   544,   546,   552,   554,   558,   570,   572,   574,   578,
     580,   582,   584,   586,   588,   590,   592,   594,   596,   598,
     600,   602,   604,   606,   608,   610,   612,   614,   616,   618,
     622,   624,   626,   628,   630,   632,   634,   636,   638,   640,
     642,   646,   648,   650,   654,   659,   663,   665,   669,   675,
     679,   681,   685,   692,   698,   703,   708,   714,   722,   730,
     735,   740,   745,   750,   761,   765,   767,   771,   772,   777,
     784,   791,   800,   807,   812,   816,   823,   832,   838,   840,
     844,   844,   854,   861,   868,   876,   888,   894,   900,   900,
     910,   910,   916,   921,   926,   928,   932,   937,   941,   949,
     949,   955,   957,   963,   965,   967,   969,   971,   973,   975,
     977,   985,   987,   997,  1003,  1006,  1012,  1018,  1020,  1022,
    1026,  1033,  1039,  1045,  1047,  1054,  1060,  1062,  1069,  1074,
    1080
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "ARG_HEAD", "ARG_TAIL", "ASCII", "BREAK",
  "CHDIR", "CMD_HEAD", "CMD_TAIL", "CONTINUE", "C_BASE", "C_EXT", "C_PATH",
  "G_BASE", "G_EXT", "G_DEXT", "G_PATH", "ELEMENT", "ELSE", "EVAL", "EXEC",
  "EXECUTE", "EXISTS", "EXIT", "FGETS", "FIELDS", "FOR", "FPRINTF",
  "GETENV", "GETCH", "GETPID", "GETS", "IDENTIFIER", "IF", "INT", "LIST",
  "LISTFIND", "LISTLEN", "LISTUNION", "MAKELIST", "ECHO_TOKEN", "NUMBER",
  "PRINTF", "PUTENV", "RETURN", "STAT", "STRCHR", "STRING", "STRINGTYPE",
  "STRLEN", "STRLWR", "RESIZE", "STRUPR", "STRFIND", "STRFORMAT", "SUBSTR",
  "SYSTEM", "TRIM", "TRIMLEFT", "TRIMRIGHT", "VOID", "WHILE", "'='",
  "AND_IS", "OR_IS", "XOR_IS", "SHL_IS", "SHR_IS", "DIV_IS", "MINUS_IS",
  "MUL_IS", "MOD_IS", "PLUS_IS", "'?'", "':'", "OR", "AND", "'|'", "'^'",
  "'&'", "EQUAL", "NOT_EQUAL", "'<'", "'>'", "SMALLER_EQUAL",
  "GREATER_EQUAL", "OLDER", "YOUNGER", "SHL", "SHR", "'+'", "'-'", "'*'",
  "'/'", "'%'", "'!'", "INC", "DEC", "'~'", "'['", "'}'", "')'", "','",
  "'`'", "']'", "'('", "';'", "'{'", "$accept", "input", "_args", "args",
  "breakOK", "breakStmnt", "closeBrace", "$@1", "closeParen", "$@2",
  "comma", "$@3", "compound", "condition", "continueStmnt", "_voidtype",
  "defVarOrFun", "enterID", "enterVarID", "errExpression", "$@4",
  "exprCode", "_p_casttype", "_string", "_func_or_var", "_backtick", "$@5",
  "expression", "_for", "_expr_list", "_opt_init_expression",
  "_opt_cond_expression", "_opt_inc_expression", "forStmnt",
  "_zero_arg_funs", "_one_arg_funs", "_two_arg_funs", "_optint_string",
  "_comma_expr", "_optint_special", "_comma_arglist", "_opt_arglist",
  "_funname", "function", "_partype", "_pars", "_opt_parlist", "_funvars",
  "_funid", "functionDef", "_if", "_else", "ifStmnt", "_makelistCall",
  "_makeList_normal", "_old_young", "_older_younger", "$@6", "makeList",
  "nesting", "ok", "openBrace", "$@7", "openParen", "$@8", "popDead",
  "pushDead", "_return_tail", "_leave", "returnStmnt", "semicol", "$@9",
  "_stm", "statement", "statements", "typedCondition", "typedVarlist",
  "_varType", "typeOfVar", "varCondition", "varExpr", "varExprList",
  "_while", "whileStmnt", "zeroSemVal", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,    61,   318,   319,   320,   321,   322,   323,
     324,   325,   326,   327,    63,    58,   328,   329,   124,    94,
      38,   330,   331,    60,    62,   332,   333,   334,   335,   336,
     337,    43,    45,    42,    47,    37,    33,   338,   339,   126,
      91,   125,    41,    44,    96,    93,    40,    59,   123
};
# endif

#define YYPACT_NINF -327

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-327)))

#define YYTABLE_NINF -221

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     -19,  -327,  -327,  -327,  -327,    21,     2,  -327,  -327,  -327,
      10,  -327,  -327,  -327,  -327,  -327,  -327,   -52,   -45,   -42,
       4,     5,  -327,  -327,   -41,  -327,   -11,   -35,  -327,  -327,
    -327,  -327,   650,    36,   -31,   332,  -327,  -327,   -23,   -15,
      36,  -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,
    -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,
    -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,    -8,  -327,
    -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,
    -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,
    -327,   650,   650,   650,   650,   650,   650,  -327,   650,   546,
      37,  -327,   775,  -327,  -327,  -327,  -327,  -327,  -327,  -327,
      -2,  -327,  -327,  -327,  -327,   -45,  -327,  -327,  -327,  -327,
    -327,  -327,  -327,  -327,  -327,  -327,     0,  -327,  -327,  -327,
     650,  -327,  -327,  -327,  -327,  -327,  -327,    -6,    -3,  -327,
    -327,  -327,  -327,    18,  -327,  -327,   -11,  -327,  -327,  -327,
    -327,  -327,  -327,  -327,   -64,   -64,   -64,   -64,   -64,   -64,
       8,    15,  -327,   775,  -327,  -327,  -327,    20,   775,  -327,
     650,   650,   650,   650,   650,   650,   650,   650,   650,   650,
     650,   650,   650,   650,   650,   650,   650,   650,   650,   650,
     650,   650,   650,   650,   650,   650,   650,   650,   650,   650,
     650,   650,  -327,  -327,   650,  -327,  -327,  -327,  -327,  -327,
      58,  -327,    60,   -47,  -327,  -327,  -327,  -327,  -327,  -327,
    -327,  -327,  -327,  -327,   775,  -327,   -21,   -11,   332,  -327,
    -327,  -327,  -327,  -327,  -327,   -11,  -327,  -327,  -327,  -327,
    -327,  -327,  -327,  -327,  -327,  -327,     1,   650,  -327,   775,
     775,   775,   775,   775,   775,   775,   775,   775,   775,   775,
     737,   318,   425,   528,   631,   795,   813,   813,    -1,    -1,
      -1,    -1,    -1,    -1,   -17,   -17,    19,    19,   -64,   -64,
     -64,   694,  -327,  -327,    12,    65,  -327,  -327,  -327,  -327,
      69,  -327,   -29,  -327,    70,  -327,  -327,  -327,  -327,  -327,
    -327,    36,  -327,  -327,  -327,  -327,  -327,   -64,   650,  -327,
    -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,
    -327,  -327,  -327,    14,   440,  -327,   109,  -327,  -327,  -327,
     229,  -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,
     650,   440,  -327,  -327,  -327,    58,  -327,   775,  -327,  -327,
    -327,    70,  -327,  -327,   155,  -327,  -327,   440,  -327,  -327,
     440,  -327,  -327,  -327,  -327
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,   207,   209,   208,    19,     0,     0,     3,   189,   210,
       0,     1,     2,   160,   180,    22,    20,     0,     0,    23,
     220,     0,    21,   216,    13,   220,   158,     0,   190,   177,
     220,   213,     0,     0,     0,     0,   204,   156,    13,     0,
       0,   181,   217,   106,   107,    99,   132,   105,   104,   121,
     120,   122,   108,   110,   111,   109,   123,   100,   136,   137,
     101,   124,   125,   180,   113,    96,    97,    98,    34,   126,
     102,   127,   180,   103,    80,   180,   112,   131,   128,    32,
     114,   116,   130,   115,   129,   180,   180,   133,   117,   118,
     119,     0,     0,     0,     0,     0,     0,    25,     0,     0,
      79,    83,   214,   180,   180,   180,   180,   180,   180,    11,
      13,   153,    23,   215,    14,     0,     8,    18,   187,   183,
     183,   186,   183,   220,   189,   161,     0,   191,   189,    27,
       0,   189,   180,   196,   180,   195,   220,     0,    25,   189,
     201,   203,   189,     0,   180,   194,     0,   178,   154,    25,
      25,    25,    25,    25,    75,    70,    77,    71,    73,    76,
       6,     0,     5,    35,    28,    29,    30,     0,    11,    31,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    72,    74,     0,   143,    25,    25,    25,    25,
      25,    33,     0,    25,   172,   200,    85,   176,   162,   218,
     192,   198,    10,   199,    26,   193,    25,    25,     0,   179,
     184,   188,   185,   197,   202,    25,   155,   159,   150,   166,
     149,   151,    13,    25,    81,    84,     0,     0,    82,    37,
      44,    45,    46,    47,    48,    40,    43,    39,    41,    42,
       0,    49,    50,    60,    61,    59,    51,    52,    54,    55,
      56,    57,    67,    66,    62,    63,    58,    64,    65,    68,
      69,     0,   144,    13,   220,   220,   140,   152,   141,    12,
      13,    13,     0,    87,    88,   189,    89,    90,    11,    16,
      17,     0,    15,    11,    25,     4,    36,    78,     0,    38,
      25,    25,   147,   135,    25,   148,   139,   170,    25,   168,
     169,   171,    25,    25,     0,    24,   211,   205,     7,    13,
      53,   145,   134,   138,    13,   167,    86,    91,   189,   182,
       0,     0,    25,    25,   174,    25,   183,   212,   182,   146,
     175,    93,    11,    94,   220,   219,     7,     0,   182,   164,
       0,   163,   165,   182,    95
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -327,  -327,  -327,  -145,  -181,  -327,   -51,  -327,  -142,  -327,
     -37,  -327,  -327,   -57,  -327,  -327,   174,   -38,  -120,   -43,
    -327,  -165,  -327,  -327,  -327,  -327,  -327,   -46,  -327,  -163,
    -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,  -327,
    -327,  -327,  -327,  -327,    39,  -327,  -327,  -327,  -327,   173,
    -327,  -327,  -327,  -327,  -149,  -327,  -130,  -327,  -327,   -91,
      73,    42,  -327,   152,  -327,  -326,  -155,  -327,  -327,  -327,
    -119,  -327,  -327,  -297,    57,  -327,     3,  -327,    13,  -327,
     161,  -327,  -327,  -327,   -20
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     5,   160,   161,   341,   124,   125,   126,   211,   212,
      33,    34,   127,   298,   128,     6,     7,    20,    21,   129,
     130,   131,   167,   100,   101,   245,   246,   102,   132,   294,
     295,   338,   352,   133,   103,   104,   105,   106,   312,   107,
     315,   287,   108,   109,    37,    38,    39,    25,    14,    15,
     134,   358,   135,   110,   214,   321,   291,   292,   111,   216,
      30,   136,   137,    26,    27,   346,   217,   231,   138,   139,
      16,    17,   140,   141,    35,   300,   142,     9,   143,   327,
      23,    24,   144,   145,    36
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      31,   146,   148,     8,   238,   221,   240,   241,     8,   223,
      42,    18,   225,    10,     1,     2,     1,     2,    10,    18,
     233,    11,   355,   234,     1,     2,   248,   339,     3,   218,
       3,   219,   362,   202,   203,    13,   204,   364,     3,    40,
    -170,  -170,     4,    19,   348,   154,   155,   156,   157,   158,
     159,   112,   163,   168,   162,    28,     1,     2,   319,   320,
     361,   293,    29,   363,  -160,   286,  -206,   -24,    32,   112,
       3,    41,   114,   213,   197,   198,   199,   200,   201,  -157,
     202,   203,     4,   204,   224,   169,  -220,   147,   195,   196,
     197,   198,   199,   200,   201,   230,   202,   203,  -142,   204,
    -167,   222,   229,   220,  -220,   306,   162,   239,   162,   162,
     242,   -13,   199,   200,   201,   311,   202,   203,   232,   204,
     244,   -92,   247,   243,   249,   250,   251,   252,   253,   254,
     255,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   324,   336,   281,    40,
    -220,   328,   289,   282,   283,   284,   285,   162,   314,   333,
     290,  -173,   340,   322,   357,   360,   323,   302,   303,    12,
     293,   326,   351,    22,   299,   236,   344,   334,   215,   237,
     288,   354,   299,   228,   113,     0,     0,     0,     0,     0,
     305,   307,     0,     0,     0,   304,   297,     0,     0,     0,
     356,     0,     0,     0,     0,   149,     0,     0,     0,   345,
       0,     0,     0,     0,   150,     0,     0,   151,     0,   296,
       0,     0,     0,     0,     0,     0,     0,   152,   153,     0,
     301,     0,     0,     0,     0,     0,   310,     0,   301,     0,
       0,     0,     0,   317,   318,   205,   206,   207,   208,   209,
     210,   329,   330,   325,   313,   316,     0,   331,   332,     0,
       0,   162,     0,     0,     0,   335,     0,     0,     0,     0,
     337,     0,     0,     0,   226,     0,   227,     0,     0,     0,
       0,     0,   342,     0,   347,     0,   235,   343,     0,   349,
     350,     0,     0,   181,     0,   182,   183,   184,   185,   186,
     187,   188,   189,   190,   191,   192,   193,   194,   195,   196,
     197,   198,   199,   200,   201,   353,   202,   203,     0,   204,
       0,     0,     0,   115,   359,   -25,   -25,   -25,   116,   -25,
     -25,   -25,   117,   -25,   -25,   -25,   -25,   -25,   -25,   -25,
     -25,     0,   -25,   -25,   -25,   -25,   118,   -25,   -25,   119,
     -25,   -25,   -25,   -25,   -25,   -25,   120,     1,     2,   -25,
     -25,   -25,   -25,   -25,   -25,   -25,   -25,   121,   -25,   -25,
     -25,     3,   -25,   -25,   -25,   -25,   -25,   -25,   -25,   -25,
     -25,   -25,   -25,     0,   122,   183,   184,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,   197,
     198,   199,   200,   201,     0,   202,   203,     0,   204,     0,
       0,     0,     0,   -25,   -25,     0,     0,     0,   -25,   -25,
     -25,   -25,   -25,    -9,     0,     0,   -25,     0,   -25,   123,
    -178,   115,     0,   -25,   -25,   -25,   116,   -25,   -25,   -25,
     117,   -25,   -25,   -25,   -25,   -25,   -25,   -25,   -25,     0,
     -25,   -25,   -25,   -25,   118,   -25,   -25,   119,   -25,   -25,
     -25,   -25,   -25,   -25,   120,     1,     2,   -25,   -25,   -25,
     -25,   -25,   -25,   -25,   -25,   121,   -25,   -25,   -25,     3,
     -25,   -25,   -25,   -25,   -25,   -25,   -25,   -25,   -25,   -25,
     -25,     0,   122,   184,   185,   186,   187,   188,   189,   190,
     191,   192,   193,   194,   195,   196,   197,   198,   199,   200,
     201,     0,   202,   203,     0,   204,     0,     0,     0,     0,
       0,   -25,   -25,     0,     0,     0,   -25,   -25,   -25,   -25,
     -25,     0,     0,     0,   -25,     0,   -25,   123,  -178,    43,
      44,    45,     0,    46,    47,    48,     0,    49,    50,    51,
      52,    53,    54,    55,    56,     0,    57,    58,    59,    60,
       0,    61,    62,     0,    63,    64,    65,    66,    67,    68,
       0,   164,   165,    69,    70,    71,    72,    73,    74,    75,
      76,     0,    77,    78,    79,   166,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,   197,
     198,   199,   200,   201,     0,   202,   203,     0,   204,     0,
       0,     0,     0,     0,     0,     0,     0,    91,    92,     0,
       0,     0,    93,    94,    95,    96,    97,     0,     0,     0,
      98,     0,    99,    43,    44,    45,     0,    46,    47,    48,
       0,    49,    50,    51,    52,    53,    54,    55,    56,     0,
      57,    58,    59,    60,     0,    61,    62,     0,    63,    64,
      65,    66,    67,    68,     0,     0,     0,    69,    70,    71,
      72,    73,    74,    75,    76,     0,    77,    78,    79,     0,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   197,   198,   199,   200,   201,     0,   202,   203,
       0,   204,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    91,    92,     0,     0,     0,    93,    94,    95,    96,
      97,     0,     0,     0,    98,     0,    99,   170,   171,   172,
     173,   174,   175,   176,   177,   178,   179,   180,   181,     0,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,   197,   198,   199,   200,   201,
       0,   202,   203,     0,   204,     0,     0,     0,     0,   309,
     170,   171,   172,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   308,   182,   183,   184,   185,   186,   187,   188,
     189,   190,   191,   192,   193,   194,   195,   196,   197,   198,
     199,   200,   201,     0,   202,   203,     0,   204,   170,   171,
     172,   173,   174,   175,   176,   177,   178,   179,   180,   181,
       0,   182,   183,   184,   185,   186,   187,   188,   189,   190,
     191,   192,   193,   194,   195,   196,   197,   198,   199,   200,
     201,     0,   202,   203,     0,   204,   187,   188,   189,   190,
     191,   192,   193,   194,   195,   196,   197,   198,   199,   200,
     201,     0,   202,   203,     0,   204,   189,   190,   191,   192,
     193,   194,   195,   196,   197,   198,   199,   200,   201,     0,
     202,   203,     0,   204
};

static const yytype_int16 yycheck[] =
{
      20,    38,    40,     0,   149,   124,   151,   152,     5,   128,
      30,     1,   131,     0,    35,    36,    35,    36,     5,     1,
     139,     0,   348,   142,    35,    36,   168,   324,    49,   120,
      49,   122,   358,    97,    98,    33,   100,   363,    49,    26,
      87,    88,    61,    33,   341,    91,    92,    93,    94,    95,
      96,    33,    98,    99,    97,   107,    35,    36,    87,    88,
     357,   226,   107,   360,   106,   210,   107,    63,    63,    33,
      49,   106,   103,   110,    91,    92,    93,    94,    95,   102,
      97,    98,    61,   100,   130,    48,   107,   102,    89,    90,
      91,    92,    93,    94,    95,   138,    97,    98,   106,   100,
     102,   101,   108,   123,   107,   104,   149,   150,   151,   152,
     153,   103,    93,    94,    95,   103,    97,    98,   138,   100,
     105,   107,   102,   160,   170,   171,   172,   173,   174,   175,
     176,   177,   178,   179,   180,   181,   182,   183,   184,   185,
     186,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,   197,   198,   199,   200,   201,   298,   322,   204,   146,
     102,   303,   102,   206,   207,   208,   209,   210,   103,   314,
     213,   102,    63,   103,    19,   356,   295,   228,   235,     5,
     345,   301,   345,    10,   227,   146,   335,   317,   115,   147,
     210,   346,   235,   136,    33,    -1,    -1,    -1,    -1,    -1,
     243,   247,    -1,    -1,    -1,   242,   226,    -1,    -1,    -1,
     352,    -1,    -1,    -1,    -1,    63,    -1,    -1,    -1,   338,
      -1,    -1,    -1,    -1,    72,    -1,    -1,    75,    -1,   226,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    85,    86,    -1,
     227,    -1,    -1,    -1,    -1,    -1,   283,    -1,   235,    -1,
      -1,    -1,    -1,   290,   291,   103,   104,   105,   106,   107,
     108,   304,   308,   301,   284,   285,    -1,   310,   311,    -1,
      -1,   314,    -1,    -1,    -1,   318,    -1,    -1,    -1,    -1,
     323,    -1,    -1,    -1,   132,    -1,   134,    -1,    -1,    -1,
      -1,    -1,   329,    -1,   340,    -1,   144,   334,    -1,   342,
     343,    -1,    -1,    74,    -1,    76,    77,    78,    79,    80,
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,   345,    97,    98,    -1,   100,
      -1,    -1,    -1,     1,   354,     3,     4,     5,     6,     7,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    -1,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    -1,    62,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    -1,    97,    98,    -1,   100,    -1,
      -1,    -1,    -1,    91,    92,    -1,    -1,    -1,    96,    97,
      98,    99,   100,   101,    -1,    -1,   104,    -1,   106,   107,
     108,     1,    -1,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    -1,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    -1,    62,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    -1,    97,    98,    -1,   100,    -1,    -1,    -1,    -1,
      -1,    91,    92,    -1,    -1,    -1,    96,    97,    98,    99,
     100,    -1,    -1,    -1,   104,    -1,   106,   107,   108,     3,
       4,     5,    -1,     7,     8,     9,    -1,    11,    12,    13,
      14,    15,    16,    17,    18,    -1,    20,    21,    22,    23,
      -1,    25,    26,    -1,    28,    29,    30,    31,    32,    33,
      -1,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    -1,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    -1,    97,    98,    -1,   100,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    91,    92,    -1,
      -1,    -1,    96,    97,    98,    99,   100,    -1,    -1,    -1,
     104,    -1,   106,     3,     4,     5,    -1,     7,     8,     9,
      -1,    11,    12,    13,    14,    15,    16,    17,    18,    -1,
      20,    21,    22,    23,    -1,    25,    26,    -1,    28,    29,
      30,    31,    32,    33,    -1,    -1,    -1,    37,    38,    39,
      40,    41,    42,    43,    44,    -1,    46,    47,    48,    -1,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    80,    81,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    92,    93,    94,    95,    -1,    97,    98,
      -1,   100,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    91,    92,    -1,    -1,    -1,    96,    97,    98,    99,
     100,    -1,    -1,    -1,   104,    -1,   106,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    -1,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      -1,    97,    98,    -1,   100,    -1,    -1,    -1,    -1,   105,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,    -1,    97,    98,    -1,   100,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      -1,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    -1,    97,    98,    -1,   100,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    -1,    97,    98,    -1,   100,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    -1,
      97,    98,    -1,   100
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    35,    36,    49,    61,   110,   124,   125,   185,   186,
     187,     0,   125,    33,   157,   158,   179,   180,     1,    33,
     126,   127,   158,   189,   190,   156,   172,   173,   107,   107,
     169,   193,    63,   119,   120,   183,   193,   153,   154,   155,
     187,   106,   193,     3,     4,     5,     7,     8,     9,    11,
      12,    13,    14,    15,    16,    17,    18,    20,    21,    22,
      23,    25,    26,    28,    29,    30,    31,    32,    33,    37,
      38,    39,    40,    41,    42,    43,    44,    46,    47,    48,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    91,    92,    96,    97,    98,    99,   100,   104,   106,
     132,   133,   136,   143,   144,   145,   146,   148,   151,   152,
     162,   167,    33,   189,   103,     1,     6,    10,    24,    27,
      34,    45,    62,   107,   114,   115,   116,   121,   123,   128,
     129,   130,   137,   142,   159,   161,   170,   171,   177,   178,
     181,   182,   185,   187,   191,   192,   119,   102,   126,   172,
     172,   172,   172,   172,   136,   136,   136,   136,   136,   136,
     111,   112,   128,   136,    35,    36,    49,   131,   136,    48,
      63,    64,    65,    66,    67,    68,    69,    70,    71,    72,
      73,    74,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    97,    98,   100,   172,   172,   172,   172,   172,
     172,   117,   118,   119,   163,   169,   168,   175,   168,   168,
     193,   179,   101,   179,   136,   179,   172,   172,   183,   108,
     128,   176,   193,   179,   179,   172,   153,   170,   112,   128,
     112,   112,   128,   119,   105,   134,   135,   102,   117,   136,
     136,   136,   136,   136,   136,   136,   136,   136,   136,   136,
     136,   136,   136,   136,   136,   136,   136,   136,   136,   136,
     136,   136,   136,   136,   136,   136,   136,   136,   136,   136,
     136,   136,   128,   128,   128,   128,   112,   150,   193,   102,
     128,   165,   166,   130,   138,   139,   185,   193,   122,   128,
     184,   187,   115,   122,   119,   128,   104,   136,    75,   105,
     119,   103,   147,   193,   103,   149,   193,   119,   119,    87,
      88,   164,   103,   179,   117,   126,   127,   188,   117,   128,
     136,   128,   128,   112,   165,   128,   130,   128,   140,   182,
      63,   113,   119,   119,   163,   179,   174,   136,   182,   128,
     128,   138,   141,   193,   175,   174,   117,    19,   160,   193,
     113,   182,   174,   182,   174
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,   109,   110,   110,   111,   111,   112,   113,   114,   116,
     115,   118,   117,   120,   119,   121,   122,   122,   123,   124,
     125,   125,   125,   126,   127,   129,   128,   130,   131,   131,
     131,   132,   132,   133,   133,   135,   134,   136,   136,   136,
     136,   136,   136,   136,   136,   136,   136,   136,   136,   136,
     136,   136,   136,   136,   136,   136,   136,   136,   136,   136,
     136,   136,   136,   136,   136,   136,   136,   136,   136,   136,
     136,   136,   136,   136,   136,   136,   136,   136,   136,   136,
     136,   136,   136,   136,   136,   137,   138,   138,   139,   139,
     139,   140,   140,   141,   141,   142,   143,   143,   143,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     145,   145,   145,   145,   145,   145,   145,   145,   145,   145,
     145,   146,   146,   146,   147,   147,   148,   148,   149,   149,
     150,   150,   151,   152,   152,   152,   152,   152,   152,   152,
     152,   152,   152,   152,   153,   154,   154,   155,   155,   156,
     157,   158,   159,   160,   160,   161,   162,   163,   164,   164,
     166,   165,   167,   167,   167,   167,   168,   169,   171,   170,
     173,   172,   174,   175,   176,   176,   177,   177,   178,   180,
     179,   181,   181,   181,   181,   181,   181,   181,   181,   181,
     181,   182,   182,   183,   183,   184,   185,   186,   186,   186,
     187,   188,   188,   189,   189,   190,   190,   190,   191,   192,
     193
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     2,     1,     3,     1,     1,     0,     1,     0,
       2,     0,     2,     0,     2,     3,     1,     1,     1,     1,
       2,     2,     2,     1,     1,     0,     2,     1,     1,     1,
       1,     2,     1,     2,     1,     0,     2,     3,     4,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     5,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       2,     2,     2,     2,     2,     2,     2,     2,     4,     1,
       1,     3,     3,     1,     3,     2,     3,     1,     1,     1,
       1,     1,     0,     1,     1,    11,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     2,     1,     1,     1,     2,     1,
       1,     1,     1,     2,     3,     5,     7,     4,     4,     3,
       3,     3,     3,     1,     2,     3,     1,     1,     0,     4,
       1,     4,     2,     2,     1,     9,     3,     0,     1,     1,
       0,     2,     2,     3,     6,     7,     1,     1,     0,     2,
       0,     2,     0,     0,     1,     1,     1,     1,     2,     0,
       2,     1,     2,     2,     1,     1,     1,     2,     2,     2,
       2,     1,     2,     2,     1,     2,     2,     1,     1,     1,
       1,     1,     3,     2,     3,     3,     1,     3,     2,     7,
       0
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YYUSE (yyoutput);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyo, yytoknum[yytype], *yyvaluep);
# endif
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyo, yytype, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &yyvsp[(yyi + 1) - (yynrhs)]
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            else
              goto append;

          append:
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return (YYSIZE_T) (yystpcpy (yyres, yystr) - yyres);
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
                    yysize = yysize1;
                  else
                    return 2;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
    default: /* Avoid compiler warnings. */
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM)
      yysize = yysize1;
    else
      return 2;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yynewstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  *yyssp = (yytype_int16) yystate;

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    goto yyexhaustedlab;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = (YYSIZE_T) (yyssp - yyss + 1);

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
# undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 4:
#line 128 "grammar"
    {
        yyval = smvAddArg(&yyvsp[-2], &yyvsp[0]);
    }
#line 1705 "parse.c"
    break;

  case 5:
#line 133 "grammar"
    {
        yyval = SVarg(&yyvsp[0]);
    }
#line 1713 "parse.c"
    break;

  case 6:
#line 140 "grammar"
    {
        yyval = smvArgs2code(&yyvsp[0]);     // convert the arguments to code
    }
#line 1721 "parse.c"
    break;

  case 7:
#line 145 "grammar"
    {
        ++gp_breakOK;
    }
#line 1729 "parse.c"
    break;

  case 8:
#line 151 "grammar"
    {
        yyval = pmv_break();
    }
#line 1737 "parse.c"
    break;

  case 9:
#line 156 "grammar"
    {
        gp_parse_error = err_closeBrace_expected;
        symtab_pop();
    }
#line 1746 "parse.c"
    break;

  case 11:
#line 163 "grammar"
    {
        gp_parse_error = err_closeParen_expected; 
    }
#line 1754 "parse.c"
    break;

  case 13:
#line 169 "grammar"
    {
        gp_parse_error = err_comma_expected; 
    }
#line 1762 "parse.c"
    break;

  case 15:
#line 178 "grammar"
    {
        yyval = SVmove(&yyvsp[-1]);
    }
#line 1770 "parse.c"
    break;

  case 18:
#line 189 "grammar"
    {
        yyval = pmv_continue();
    }
#line 1778 "parse.c"
    break;

  case 19:
#line 195 "grammar"
    {
        gp_varType = 0;
    }
#line 1786 "parse.c"
    break;

  case 20:
#line 202 "grammar"
    {
        svCatenate(&gp_init, &yyvsp[-1]);
        }
#line 1794 "parse.c"
    break;

  case 23:
#line 213 "grammar"
    {
        p_defineVar();    /* the first n variables of a function, up to the
                            end of the parameter list are the parameters. */
    }
#line 1803 "parse.c"
    break;

  case 24:
#line 220 "grammar"
    {
        yyval = pmv_fetchVar();
    }
#line 1811 "parse.c"
    break;

  case 25:
#line 226 "grammar"
    {
        gp_parse_error = err_in_expression;
    }
#line 1819 "parse.c"
    break;

  case 26:
#line 230 "grammar"
    {
        yyval = SVmove(&yyvsp[0]);
    }
#line 1827 "parse.c"
    break;

  case 27:
#line 236 "grammar"
    {
        yyval = pmv_expression(&yyvsp[0]);
    }
#line 1835 "parse.c"
    break;

  case 31:
#line 250 "grammar"
    {                                       
        ssAppendBuf(util_string());         // append the next string 
    }
#line 1843 "parse.c"
    break;

  case 32:
#line 255 "grammar"
    {
        ssSetBuf(util_string());            // assign the 1st string
    }
#line 1851 "parse.c"
    break;

  case 34:
#line 264 "grammar"
    {
        msg("identifier: %s", util_string());
        yyval = pmv_fetchVar();
    }
#line 1860 "parse.c"
    break;

  case 35:
#line 271 "grammar"
    {
        gp_parse_error = err_backtick_expected;
    }
#line 1868 "parse.c"
    break;

  case 37:
#line 279 "grammar"
    {
        msg("inc/expression.5: assignment (=)");
        yyval = pmv_assign(&yyvsp[-2], &yyvsp[0]);
    }
#line 1877 "parse.c"
    break;

  case 38:
#line 285 "grammar"
    {
        yyval = pmv_indexOp(&yyvsp[-3], &yyvsp[-1]);
    }
#line 1885 "parse.c"
    break;

  case 39:
#line 290 "grammar"
    {
        yyval = pmv_compoundAss(&yyvsp[-2], &yyvsp[0], pmv_multiply, "*=");
    }
#line 1893 "parse.c"
    break;

  case 40:
#line 295 "grammar"
    {
        yyval = pmv_compoundAss(&yyvsp[-2], &yyvsp[0], pmv_divide, "/=");
    }
#line 1901 "parse.c"
    break;

  case 41:
#line 300 "grammar"
    {
        yyval = pmv_compoundAss(&yyvsp[-2], &yyvsp[0], pmv_modulo, "%=");
    }
#line 1909 "parse.c"
    break;

  case 42:
#line 305 "grammar"
    {
        yyval = pmv_compoundAss(&yyvsp[-2], &yyvsp[0], pmv_addition, "+=");
    }
#line 1917 "parse.c"
    break;

  case 43:
#line 310 "grammar"
    {
        yyval = pmv_compoundAss(&yyvsp[-2], &yyvsp[0], pmv_subtract, "-=");
    }
#line 1925 "parse.c"
    break;

  case 44:
#line 315 "grammar"
    {
        yyval = pmv_compoundAss(&yyvsp[-2], &yyvsp[0], pmv_bitand, "&=");
    }
#line 1933 "parse.c"
    break;

  case 45:
#line 320 "grammar"
    {
        yyval = pmv_compoundAss(&yyvsp[-2], &yyvsp[0], pmv_bitor, "|=");
    }
#line 1941 "parse.c"
    break;

  case 46:
#line 325 "grammar"
    {
        yyval = pmv_compoundAss(&yyvsp[-2], &yyvsp[0], pmv_xor, "^=");
    }
#line 1949 "parse.c"
    break;

  case 47:
#line 330 "grammar"
    {
        yyval = pmv_compoundAss(&yyvsp[-2], &yyvsp[0], pmv_shl, "<<=");
    }
#line 1957 "parse.c"
    break;

  case 48:
#line 335 "grammar"
    {
        yyval = pmv_compoundAss(&yyvsp[-2], &yyvsp[0], pmv_shr, ">>=");
    }
#line 1965 "parse.c"
    break;

  case 49:
#line 340 "grammar"
    {
        yyval = pmv_or(&yyvsp[-2], &yyvsp[0]);
    }
#line 1973 "parse.c"
    break;

  case 50:
#line 345 "grammar"
    {
        yyval = pmv_and(&yyvsp[-2], &yyvsp[0]);
    }
#line 1981 "parse.c"
    break;

  case 51:
#line 350 "grammar"
    {
        yyval = pmv_equal(&yyvsp[-2], &yyvsp[0]);
    }
#line 1989 "parse.c"
    break;

  case 52:
#line 355 "grammar"
    {
        yyval = pmv_notEqual(&yyvsp[-2], &yyvsp[0]);
    }
#line 1997 "parse.c"
    break;

  case 53:
#line 360 "grammar"
    {
        yyval = pmv_ternary(&yyvsp[-4], &yyvsp[-2], &yyvsp[0]);
    }
#line 2005 "parse.c"
    break;

  case 54:
#line 365 "grammar"
    {
        yyval = pmv_smaller(&yyvsp[-2], &yyvsp[0]);
    }
#line 2013 "parse.c"
    break;

  case 55:
#line 370 "grammar"
    {
        yyval = pmv_greater(&yyvsp[-2], &yyvsp[0]);
    }
#line 2021 "parse.c"
    break;

  case 56:
#line 375 "grammar"
    {
        yyval = pmv_smEqual(&yyvsp[-2], &yyvsp[0]);
    }
#line 2029 "parse.c"
    break;

  case 57:
#line 380 "grammar"
    {
        yyval = pmv_grEqual(&yyvsp[-2], &yyvsp[0]);
    }
#line 2037 "parse.c"
    break;

  case 58:
#line 385 "grammar"
    {
        yyval = pmv_addition(&yyvsp[-2], &yyvsp[0]);
    }
#line 2045 "parse.c"
    break;

  case 59:
#line 390 "grammar"
    {
        yyval = pmv_bitand(&yyvsp[-2], &yyvsp[0]);
    }
#line 2053 "parse.c"
    break;

  case 60:
#line 395 "grammar"
    {
        yyval = pmv_bitor(&yyvsp[-2], &yyvsp[0]);
    }
#line 2061 "parse.c"
    break;

  case 61:
#line 400 "grammar"
    {
        yyval = pmv_xor(&yyvsp[-2], &yyvsp[0]);
    }
#line 2069 "parse.c"
    break;

  case 62:
#line 405 "grammar"
    {
        yyval = pmv_shl(&yyvsp[-2], &yyvsp[0]);
    }
#line 2077 "parse.c"
    break;

  case 63:
#line 410 "grammar"
    {
        yyval = pmv_shr(&yyvsp[-2], &yyvsp[0]);
    }
#line 2085 "parse.c"
    break;

  case 64:
#line 415 "grammar"
    {
        yyval = pmv_subtract(&yyvsp[-2], &yyvsp[0]);
    }
#line 2093 "parse.c"
    break;

  case 65:
#line 420 "grammar"
    {
        yyval = pmv_multiply(&yyvsp[-2], &yyvsp[0]);
    }
#line 2101 "parse.c"
    break;

  case 66:
#line 425 "grammar"
    {
        yyval = pmv_young(&yyvsp[-2], &yyvsp[0]);
    }
#line 2109 "parse.c"
    break;

  case 67:
#line 430 "grammar"
    {
        yyval = pmv_old(&yyvsp[-2], &yyvsp[0]);
    }
#line 2117 "parse.c"
    break;

  case 68:
#line 435 "grammar"
    {
        yyval = pmv_divide(&yyvsp[-2], &yyvsp[0]);
    }
#line 2125 "parse.c"
    break;

  case 69:
#line 440 "grammar"
    {
        yyval = pmv_modulo(&yyvsp[-2], &yyvsp[0]);
    }
#line 2133 "parse.c"
    break;

  case 70:
#line 445 "grammar"
    {
        yyval = pmv_negate(&yyvsp[0]);
    }
#line 2141 "parse.c"
    break;

  case 71:
#line 450 "grammar"
    {
        yyval = pmv_incDec(pre_op, op_inc, &yyvsp[0]);
    }
#line 2149 "parse.c"
    break;

  case 72:
#line 455 "grammar"
    {
        yyval = pmv_incDec(post_op, op_inc, &yyvsp[-1]);
    }
#line 2157 "parse.c"
    break;

  case 73:
#line 460 "grammar"
    {
        yyval = pmv_incDec(pre_op, op_dec, &yyvsp[0]);
    }
#line 2165 "parse.c"
    break;

  case 74:
#line 465 "grammar"
    {
        yyval = pmv_incDec(post_op, op_dec, &yyvsp[-1]);
    }
#line 2173 "parse.c"
    break;

  case 75:
#line 470 "grammar"
    {
        yyval = SVmove(&yyvsp[0]);
    }
#line 2181 "parse.c"
    break;

  case 76:
#line 476 "grammar"
    {
        yyval = pmv_compl(&yyvsp[0]);
    }
#line 2189 "parse.c"
    break;

  case 77:
#line 481 "grammar"
    {
        yyval = pmv_not(&yyvsp[0]);
    }
#line 2197 "parse.c"
    break;

  case 78:
#line 486 "grammar"
    {
        yyval = pmv_cast(svType(&yyvsp[-2]), &yyvsp[0]);
    }
#line 2205 "parse.c"
    break;

  case 79:
#line 491 "grammar"
    {
        yyval = SVtype(e_str | e_const, ssBuf());
    }
#line 2213 "parse.c"
    break;

  case 80:
#line 496 "grammar"
    {
        yyval = SVtype(e_int | e_const, util_string());
    }
#line 2221 "parse.c"
    break;

  case 81:
#line 501 "grammar"
    {
        msg("[ list ]");
        yyval = pmv_listConst(&yyvsp[-1]);
    }
#line 2230 "parse.c"
    break;

  case 82:
#line 507 "grammar"
    {
        yyval = SVmove(&yyvsp[-1]);
    }
#line 2238 "parse.c"
    break;

  case 84:
#line 514 "grammar"
    {
        yyval = pmv_oneArg(f_backtick, &yyvsp[-1]);
    }
#line 2246 "parse.c"
    break;

  case 85:
#line 520 "grammar"
    {
        symtab_push();
    }
#line 2254 "parse.c"
    break;

  case 86:
#line 527 "grammar"
    {
        svCatenate(&yyvsp[-2], &yyvsp[0]);
        yyval = SVmove(&yyvsp[-2]);
    }
#line 2263 "parse.c"
    break;

  case 92:
#line 546 "grammar"
    {
        yyval = SVint(1);
    }
#line 2271 "parse.c"
    break;

  case 95:
#line 565 "grammar"
    {
        yyval = pmv_for(&yyvsp[-8], &yyvsp[-6], &yyvsp[-4], &yyvsp[-1]);
    }
#line 2279 "parse.c"
    break;

  case 134:
#line 655 "grammar"
    {
        yyval = SVmove(&yyvsp[0]);
    }
#line 2287 "parse.c"
    break;

  case 138:
#line 671 "grammar"
    {                                   /* README.args          */
        yyval = SVmove(&yyvsp[0]);         
    }
#line 2295 "parse.c"
    break;

  case 142:
#line 686 "grammar"
    {
        yyval = SVint(p_functionIdx());
    }
#line 2303 "parse.c"
    break;

  case 143:
#line 694 "grammar"
    {
        yyval = pmv_zeroArgs(svType(&yyvsp[-1]));
    }
#line 2311 "parse.c"
    break;

  case 144:
#line 699 "grammar"
    {
        yyval = pmv_oneArg(svType(&yyvsp[-2]), &yyvsp[0]);
    }
#line 2319 "parse.c"
    break;

  case 145:
#line 704 "grammar"
    {
        yyval = pmv_twoArgs(svType(&yyvsp[-4]), &yyvsp[-2], &yyvsp[0]);
    }
#line 2327 "parse.c"
    break;

  case 146:
#line 710 "grammar"
    {
        yyval = pmv_threeArgs(yyvsp[-6].type, &yyvsp[-4], &yyvsp[-2], &yyvsp[0]);
    }
#line 2335 "parse.c"
    break;

  case 147:
#line 718 "grammar"
    {
        yyval = pmv_optIntString(svType(&yyvsp[-3]), &yyvsp[-1], &yyvsp[0]);
    }
#line 2343 "parse.c"
    break;

  case 148:
#line 726 "grammar"
    {
        yyval = pmv_checkSpecial(svType(&yyvsp[-3]), &yyvsp[-1], &yyvsp[0]);
    }
#line 2351 "parse.c"
    break;

  case 149:
#line 731 "grammar"
    {
        yyval = pmv_specials(f_printf, &yyvsp[0]);
    }
#line 2359 "parse.c"
    break;

  case 150:
#line 736 "grammar"
    {
        yyval = pmv_fprintf(svType(&yyvsp[-2]), &yyvsp[0]);
    }
#line 2367 "parse.c"
    break;

  case 151:
#line 741 "grammar"
    {
        yyval = pmv_specials(f_strformat, &yyvsp[0]);
    }
#line 2375 "parse.c"
    break;

  case 152:
#line 746 "grammar"
    {
        yyval = pmv_callFunction(svValue(&yyvsp[-2]), &yyvsp[0]);
    }
#line 2383 "parse.c"
    break;

  case 159:
#line 778 "grammar"
    {
        symtab_setFunParams(); /* the # variables so far are the parameters */
    }
#line 2391 "parse.c"
    break;

  case 160:
#line 785 "grammar"
    {
        p_beginFunction();
    }
#line 2399 "parse.c"
    break;

  case 161:
#line 795 "grammar"
    {
        p_endFunction(&yyvsp[-1]);
    }
#line 2407 "parse.c"
    break;

  case 162:
#line 801 "grammar"
    {
        symtab_push();
    }
#line 2415 "parse.c"
    break;

  case 163:
#line 808 "grammar"
    {
        yyval = SVmove(&yyvsp[0]);
    }
#line 2423 "parse.c"
    break;

  case 165:
#line 817 "grammar"
    {
        yyval = pmv_if(&yyvsp[-6], &yyvsp[-4], &yyvsp[-1]);
        symtab_pop();
    }
#line 2432 "parse.c"
    break;

  case 166:
#line 826 "grammar"
    {
        yyval = SVmove(&yyvsp[0]);
    }
#line 2440 "parse.c"
    break;

  case 167:
#line 832 "grammar"
    {
        yyval = SVint(IS_FILE);
    }
#line 2448 "parse.c"
    break;

  case 170:
#line 844 "grammar"
    {
        gp_parse_error = err_older_younger; 
    }
#line 2456 "parse.c"
    break;

  case 171:
#line 848 "grammar"
    {
        yyval = SVmove(&yyvsp[0]);
    }
#line 2464 "parse.c"
    break;

  case 172:
#line 856 "grammar"
    {
        yyval = pmv_makeList1(&yyvsp[-1], &yyvsp[0]); // makelist of a file regex

    }
#line 2473 "parse.c"
    break;

  case 173:
#line 864 "grammar"
    {
        yyval = pmv_makeList1(&yyvsp[0], &yyvsp[-2]);
    }
#line 2481 "parse.c"
    break;

  case 174:
#line 872 "grammar"
    {
        yyval = pmv_makeList2(svType(&yyvsp[-3]), &yyvsp[0], &yyvsp[-5], &yyvsp[-1]);
    }
#line 2489 "parse.c"
    break;

  case 175:
#line 880 "grammar"
    {
        yyval = pmv_makeList2(svType(&yyvsp[-2]), &yyvsp[-6], &yyvsp[-4], &yyvsp[0]);
    }
#line 2497 "parse.c"
    break;

  case 176:
#line 889 "grammar"
    {
        ++gp_nestLevel;
    }
#line 2505 "parse.c"
    break;

  case 177:
#line 895 "grammar"
    {
        yyerrok;
    }
#line 2513 "parse.c"
    break;

  case 178:
#line 900 "grammar"
    {
        gp_parse_error = err_openBrace_expected;
    }
#line 2521 "parse.c"
    break;

  case 179:
#line 904 "grammar"
    {
        symtab_push();
    }
#line 2529 "parse.c"
    break;

  case 180:
#line 910 "grammar"
    {
        gp_parse_error = err_openParen_expected;
    }
#line 2537 "parse.c"
    break;

  case 182:
#line 916 "grammar"
    {
        deadPop();
    }
#line 2545 "parse.c"
    break;

  case 183:
#line 921 "grammar"
    {
        deadPush();                     // set new dead-level
    }
#line 2553 "parse.c"
    break;

  case 186:
#line 933 "grammar"
    {
        msg("saw return");
    }
#line 2561 "parse.c"
    break;

  case 188:
#line 943 "grammar"
    {
        yyval = pmv_return(svType(&yyvsp[-1]), &yyvsp[0]);
        msg("SAW return stmt");
    }
#line 2570 "parse.c"
    break;

  case 189:
#line 949 "grammar"
    {
        gp_parse_error = err_semicol_expected; 
    }
#line 2578 "parse.c"
    break;

  case 192:
#line 959 "grammar"
    {
        yyval = SVmove(&yyvsp[0]);
    }
#line 2586 "parse.c"
    break;

  case 200:
#line 978 "grammar"
    {
        yyval = SVint(0);
        svPushStack(&yyval);
    }
#line 2595 "parse.c"
    break;

  case 203:
#line 999 "grammar"
    {
        yyval = pmv_catStmnts(&yyvsp[-1], &yyvsp[0]);
    }
#line 2603 "parse.c"
    break;

  case 205:
#line 1007 "grammar"
    {
        yyval = SVmove(&yyvsp[0]);
    }
#line 2611 "parse.c"
    break;

  case 206:
#line 1013 "grammar"
    {
        yyval = SVmove(&yyvsp[0]);
    }
#line 2619 "parse.c"
    break;

  case 210:
#line 1027 "grammar"
    {
        gp_parse_error = err_identifier_expected;
        gp_varType = svType(&yyvsp[0]);
    }
#line 2628 "parse.c"
    break;

  case 211:
#line 1034 "grammar"
    {
        svAddCode(&yyvsp[0], op_push_imm, 0);
        yyval = SVmove(&yyvsp[0]);
    }
#line 2637 "parse.c"
    break;

  case 212:
#line 1040 "grammar"
    {
        yyval = pmv_assign(&yyvsp[-2], &yyvsp[0]);    /* explicit initialization */
    }
#line 2645 "parse.c"
    break;

  case 214:
#line 1048 "grammar"
    {
        msg("inc/varexpr.5: init variable");
        yyval = pmv_expressionAssign(&yyvsp[-2], &yyvsp[0]);  // explicit initialization
    }
#line 2654 "parse.c"
    break;

  case 215:
#line 1055 "grammar"
    {
        svCatenate(&yyvsp[-2], &yyvsp[0]);       // catenate variable initialization code
        yyval = SVmove(&yyvsp[-2]);
    }
#line 2663 "parse.c"
    break;

  case 217:
#line 1063 "grammar"
    {
        yyval = SVmove(&yyvsp[0]);
    }
#line 2671 "parse.c"
    break;

  case 219:
#line 1075 "grammar"
    {
        yyval = pmv_while(&yyvsp[-4], &yyvsp[-1], 1);
    }
#line 2679 "parse.c"
    break;

  case 220:
#line 1080 "grammar"
    {
        yyval = SVzero();   // by default initializes a frame to 0
    }
#line 2687 "parse.c"
    break;


#line 2691 "parse.c"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;


#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif


/*-----------------------------------------------------.
| yyreturn -- parsing is finished, return the result.  |
`-----------------------------------------------------*/
yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 1085 "grammar"


int yywrap(void)
{
    return 1;
}
/*
    stringlist:
        stringlist ',' _string
    |
        _string
    ;
    
    varExpr:
        enterID
        zeroSemVal
    |
        enterVarID
        '='
        expression
        {
            $$ = *p_expression(p_assign(&$1, &$3));
        }
    |
        enterVarID
        '=' '{' stringlist '}'
    ;
*/

