#include "parser.ih"

int p_oneArgFunTest(ExprType *type, SemVal const *arg)
{
    switch ((FunNr)*type)
    {
        case f_ascii_int:
            if (svTestType(arg, e_str | e_int))
            {
                if (svTestType(arg, e_int))  // int received    
                    *type = f_ascii_str;     // string returned 
                return 1;
            }
        return 0;

        case f_listlen:
            if (strcmp(scanner_savedText(), "listlen") != 0)
                rss_warning(util_sourceName(), 
                            scanner_savedLineNr(), 
                            "`%s' is deprecated. Use `listlen'\n", 
                            scanner_savedText());

        return svTestType(arg, e_list);

        case f_echo:
        return svTestType(arg, e_int);

        default:
            // case f_backtick:
            // case f_getenv:
            // case f_putenv:
            // case f_eval:
            // case f_exists:
            // case f_cmd_head:
            // case f_cmd_tail:
            // case f_arg_head:
            // case f_arg_tail:
            // case f_g_path:
            // case f_g_base:
            // case f_g_ext:
            // case f_strlen:
            // case f_strlwr:
            // case f_strupr:
            // case f_trim:
            // case f_trimright:
            // case f_trimleft:
       
        return svTestType(arg, e_str);
    }
}
