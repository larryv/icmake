#include "parser.ih"

int yyerror(char *s)
{
    if (!yytext[0])
        rss_fatal(util_sourceName(), yylineno, 
                    "Unexpected end of file.");

    if (gp_errMsg[gp_parse_error].count > 0)
    {
        rss_error(util_sourceName(), yylineno,
                    "at '%s': '%s'%s", 
                    yytext, gp_errMsg[gp_parse_error].msg,
                    gp_parse_error == err_in_expression ?
                        ""
                    :
                        " expected");
        --gp_errMsg[gp_parse_error].count;
    }

    return 0;
}
