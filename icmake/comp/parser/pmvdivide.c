#include "parser.ih"

SemVal pmv_divide(SemVal *lval, SemVal *rval)
{
    if (p_testBinOp(op_div, lval, rval))
        return pmv_semVal(2, lval, rval);     // test for correct types 

    svBool2int(lval);                       // convert pending booleans 
    svBool2int(rval);

    if (p_conflict(lval, rval, op_div))     // test type p_conflict 
        return pmv_semVal(2, lval, rval);

    if (svTestType(rval, e_const))
    {
        if (svValue(rval) == 0)             // expression / 0: not allowed 
        {
            util_semantic(sem_zeroDiv);
            return pmv_semVal(2, lval, rval);
        }

        if (svTestType(lval, e_const))
        {
            svSetValue(lval, svValue(lval) / svValue(rval));
            return SVmove(lval);
        }
    }

    return pmv_binOp(lval, rval, op_div);     // return new expression 
}



