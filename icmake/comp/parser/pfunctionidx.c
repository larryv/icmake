#include "parser.ih"

int p_functionIdx()
{
    register int idx = symtab_findFun();

    if (idx == -1)
        util_semantic(sem_funNotDefined, util_string());

    return idx;    
}
