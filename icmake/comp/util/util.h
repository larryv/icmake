#ifndef INCLUDED_UTIL_H_
#define INCLUDED_UTIL_H_

#include <stdio.h>

    // initialization of error-count values: #times an error can be reported
    // inside a function definition
#define ERRCOUNT    5

typedef enum                        // update data.c when this enum changes
{
    sem_noMain,
    sem_mainNotVoid,
    sem_multiplyDefined,
    sem_funArgTypeMismatch,
    sem_funNotDefined,
    sem_noBreak,
    sem_argCount,
    sem_noContinue,
    sem_zeroDiv,
    sem_undefined,
    sem_noModulo0,
    sem_returnType,
    sem_typeConflict,   
    sem_illegalType,    
    sem_lvalueNeeded,   
    sem_illegalArgCount,
    sem_illegalCast,    
    sem_stringsRequired,
    
    semCOUNT

} SEMANTIC_ERR;

typedef struct MsgCount_
{
    unsigned count;
    char const *msg;
} MsgCount;


char const *util_sourceName(void);
char const *util_string(void);

int         util_printf(char const *fmt, ...);  /* fill gu_lexstring */

void        util_catString(char const *txt);
void        util_out(FILE *bin, void const *source, unsigned sourceSizeInBytes);
void        util_resetSemErr(void);
void        util_semantic(SEMANTIC_ERR type, ...);
void        util_setSourceName(char const *txt);
void        util_setString(char const *txt);
void        util_scan(char const *fmt, char const *text);

#endif


