#include "semval.ih"

void svPatchupTrue(SemVal *sv, int pos)
{
    sp_patchup(sv, sv->truelist, sv->truelen, pos);
    sv->truelen = 0;
}
