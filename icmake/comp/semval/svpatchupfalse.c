#include "semval.ih"

void svPatchupFalse(SemVal *sv, int pos)
{
    sp_patchup(sv, sv->falselist, sv->falselen, pos);
    sv->falselen = 0;
}
