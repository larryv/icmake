//#define msgx

#include "semval.ih"

void sp_downType(SemVal *sv, int exprTypeOrValue)
{
    sv->type &= ~exprTypeOrValue;
}
