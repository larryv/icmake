//#define msgx
#include "parser.ih"

SemVal pmv_oneArg(ExprType type, SemVal *arg)
{
    msg("calling function 0x%x, arg type = %x", type, svType(arg));
    svOut(arg);

    svPushStack(arg);                               // arg to stack 

    msg("after pushstack: type = %x", svType(arg));
    svOut(arg);

    if (p_oneArgFunTest(&type, arg))
        p_callRss(arg, type);
    else
        util_semantic(sem_typeConflict, gp_funstring[type]);

    msg("END");
    return SVmove(arg);
}
