#include "util.ih"

void util_resetSemErr(void)
{
    for (unsigned idx = 0; idx != semCOUNT; ++idx)
        gu_semantic[idx].count = ERRCOUNT;
}
