//#define msgx
#include "parser.ih"

SemVal pmv_while(SemVal *expr, SemVal *stmnt, int pureWhile)
{
    unsigned len;
    unsigned *list;

    msg("BEGIN");

    --gp_nestLevel;                     // reduce nesting level 
    --gp_breakOK;                       // reduce break ok 

    p_expr2bool(expr);                  // make links for EXPR 

    msg("1");

    if (svTestType(expr, e_const))      // constant: never xeq 
    {
        msg("2");

        if (svValue(expr) == 0)         // const 0 value: ignore the while 
            return pmv_semVal(2, expr, stmnt);

        svSetValue(expr, 0);            // no value means no code for 
                                        // svCatenate 
    }

    msg("3");

    svPatchupTrue(expr, 1);             // patch to EOC 

    msg("4");

    svGrabFalse(&list, &len, expr);     // grab the falselist

    msg("5");

    svCatenate(expr, stmnt);            // append stmnt to expr 

    msg("6");

    if (pureWhile)                      // not a while stmt which is part 
        svPatchupContinue(expr, -svCodeLength(expr));  //       of a for stmnt

    msg("7");

    svAddCode(expr, op_jmp, j_falselist);  // jmp to begin of code 

    msg("8");

    svPatchupFalse(expr, 0);            // patch to BOC 

    msg("9");

    svSetFalse(expr, list, len);        // restore the expr's falselist

    msg("a");

    svPatchupTrue(expr, 1);             // pmv_patchup break targets to EOC 

    msg("b");

    return SVmove(expr);
}





