#include "parser.ih"

SemVal pmv_cast(ExprType target, SemVal *sv)
{
    svBool2int(sv);                                // convert boolean to int 

    switch (target)
    {
        case e_int:                        // pmv_cast to ints 
            p_iCast(sv);
        break;

        case e_str:                        // pmv_cast to strings 
            p_sCast(sv);
        break;

        case e_list:                       // pmv_cast to lists 
            p_lCast(sv);
        break;

        default:
            // default is entered in the switch to prevent a long compiler
            // warning 
        break;
    }

    return SVmove(sv);
}
