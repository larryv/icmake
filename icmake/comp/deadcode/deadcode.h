#ifndef INCLUDED_DEADCODE_
#define INCLUDED_DEADCODE_

void deadInit(void);                // initialize (by parser.c)
void deadReset();                   // reset to level 0, no dead code
void deadZero();                    // current level set to 0
void deadPush(void);                // new dead level
void deadPush0(void);               // new dead level, not dead
void deadPop(void);                 // restore dead level
void deadInc(void);                 // increment the dead level
int  deadCode();                    // 0: code not dead. Otherwise: dead

#endif

