typedef enum
{
    j_uncond,                               // unconditional jump 
    j_truelist,                             // jump batchpatch for truelist 
    j_falselist,                            // jump backbatch for falselist 
    j_continuelist                          // jump backp. for continuelist 
} JMP_COND_;

