//#define msgx

#include "semval.ih"

            // args is a Semval argument structure
unsigned sp_argsCodelen(SemVal *args, unsigned begin, unsigned count)
{
    unsigned codelen = 0;

    SemVal const *src = svArg(args, begin);     // first SemVal struct
                                                // to consider

    for (unsigned idx = 0; idx != count; ++idx) // compute the required code
    {                                           // length
        codelen += src->codelen;
        ++src;                                  // shift to the next SemVal
    }

    return codelen;
}
