#include "icmake.ih"

void about(char const *program)
{
    rss_copyright(program);

    printf("%s%s",

    "ICMAKE consists of a set of five programs. Together, they can\n"
    "be used for managing program development comparable to, e.g.,\n"
    "the UNIX make facility, or as a SHELL-script language.\n"
    "\n"
    "Icmake was developed using the C programming language by Frank Brokken\n"
    "and (until version 6.xx) Karel Kubat\n"
    "\n"
    "ICMAKE is available on various UNIX-platforms.\n"
    "\n",

    "ICMAKE's home page is:\n"
    "    http://fbb-git.gitlab.io/icmake/\n"
    "Also, ICMAKE is included in the Debian Linux distribution\n"
    "\n"
    "Questions, remarks, etc. about ICMAKE can be sent to:\n"
    "\n"
    "              Frank B. Brokken,\n"
    "phone:        (+31) 50 363 9281\n"
    "e-mail:       f.b.brokken@rug.nl\n"
    "\n"
    "              University of Groningen\n"
    "              the Netherlands"
    "\n");

    exit(0);
}








