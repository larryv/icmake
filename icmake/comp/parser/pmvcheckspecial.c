//#define msgx

#include "parser.ih"

// see README.args for details
//  *larg: any expression
//  *rarg: SemVal argument structure

SemVal pmv_checkSpecial(ExprType type, SemVal *larg, SemVal *rarg)
{
    msg("BEGIN");

    int firstInt = svTestType(larg, e_int);

    svPushStack(larg);                          // larg's value to the stack 

        // if larg is an int then it is P_(NO)CHECK and larg is converted
        // to a SemVal arg structure
    if (firstInt)
        *larg = SVarg(larg);
    else                        // If not an int then convert larg to a 
    {                           // SemVal arg. structure having 0 as 1st
                                // argument, indicatingP_CHECK (the default).
                                // and the original larg as its 2nd argument

        SemVal tmp = SVint(0);                  
        svPushStack(&tmp);                      // push the arg to the stack

        tmp = SVarg(&tmp);                      // tmp now a SemVal argument
                                                // structure

            // larg is the lhs arg. struct: first the 0, then larg itself
        *larg = smvAddArg(&tmp, larg);           
        
    }    

    *larg = smvJoinArgs(larg, rarg);             // add rarg's args to larg

    msg("#args: %d", svValue(larg));

    return pmv_specials(type, larg);
}

