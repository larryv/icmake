//#define msgx

#include "semval.ih"

void svDestroy(SemVal *sv)
{
    if (sv != NULL)
    {
        free(sv->falselist);
        free(sv->truelist);
        free(sv->code);
        *sv = SVzero();
    }
}
