// p_makeFrame writes the op_frame instruction, defining the number and
// types of all local variables, to gp_bin. 
// p_makeFrame is called by p_endFunction, which writes the statements of the
// functions to gp_bin after calling p_makeFrame.


//#define msg

#include "parser.ih"

void p_makeFrame()
{
    if (symtab_nLocals())
    {
        SemVal tmp = SVzero();

        deadPush0();
        svAddCode(&tmp, op_frame);    // generate frame instruction
        deadPop();

        util_out(gp_bin, (char const *)svCode(&tmp),    // write to gp_bin 
                         svCodeLength(&tmp));

        msg("write code frame of %u bytes", svCodeLength(&tmp));

        svDestroy(&tmp);
    }
}
