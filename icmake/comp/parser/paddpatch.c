#include "parser.ih"

void p_addPatch(unsigned *list, unsigned len, register unsigned value)
{
    for (register unsigned idx = 0; idx < len; ++idx)
        list[idx] += value;
}
