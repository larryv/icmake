//#define msgx
#include "parser.ih"

SemVal pmv_binOp(SemVal *lval, SemVal *rval, Opcode opcode)
{
    msg("BEGIN");

    svPushStack(lval);                  // convert to code 
    svPushStack(rval);
    
    msg("lval len = %d, rval len = %d", svCodeLength(lval), \
                                        svCodeLength(rval));
                                        
    svCatenate(lval, rval);

    msg("post lval len = %d, opcode = %x", svCodeLength(lval), opcode);

    svAddCode(lval, opcode);            // append instruction 
    svSetType(lval, e_int | e_stack);   // set appropriate type 

    msg("exit lval len = %d, opcode = %x", svCodeLength(lval), opcode);
    return SVmove(lval);
}
