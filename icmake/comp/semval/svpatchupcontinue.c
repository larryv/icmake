#include "semval.ih"

void svPatchupContinue(SemVal *sv, int pos)
{
    sp_patchup(sv, sv->continuelist, sv->continuelen, pos);
    sv->continuelen = 0;
}
