#include "parser.ih"

SemVal pmv_addition(SemVal *lval, SemVal *rval)
{
    register ExprType type;

    if (p_testBinOp(op_add, lval, rval))
        return pmv_semVal(2, lval, rval);     // test for correct types 

    svBool2int(lval);                       // convert pending booleans 
    svBool2int(rval);

    if (p_conflict(lval, rval, op_add))     // test type p_conflict 
        return pmv_semVal(2, lval, rval);

    type = svType(lval);                      // keep type for later 

    if ((type & svType(rval) & (unsigned)~e_typeMask) == e_const)
    {
        if (svTestType(lval, e_int))
            svSetValue(lval, svValue(lval) + svValue(rval));
        else if (svTestType(lval, e_str))
            p_catStrings(lval, rval);         // create (cat) new string 
    }
    else 
    {
        *lval = pmv_binOp(lval, rval, op_add);
        svSetType(lval, (type & e_typeMask) | e_stack);
    }

    return SVmove(lval);               // return new expression 
}


