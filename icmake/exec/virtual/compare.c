#include "virtual.ih"

    // stack top:     rhs
    // stack top- :   lhs

void virtual_compare()
{
    Variable rval;
    copycons(&rval, stack_top());               // save the rhs's value
    stack_pop();                                // remove it fm the stack_top

    Variable *lhs = stack_top();                // directly acces the lhs

    p_compare[var_typeValue(lhs)](lhs, &rval);  // compare the two values
                                                // storing the result at the
                                                // stack_top

    destructor(&rval);                          // rval not needed anymore

    // on exit the stack-top contains -1: lhs <  rhs, 
    //                                 0: lhs == rhs
    //                                 1: lhs > rhs
}

