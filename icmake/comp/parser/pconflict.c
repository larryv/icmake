#include "parser.ih"

int p_conflict(SemVal const *lval, SemVal const *rval, Opcode opcode) 
{
    register int ret = !(svType(lval) & svType(rval) & gp_opType[opcode]);

    if (ret)
        util_semantic(sem_typeConflict, gp_opstring[opcode]);

    return ret;
}
