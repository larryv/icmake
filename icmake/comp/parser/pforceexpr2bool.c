// Force conversion of expression to boolean expression 

#include "parser.ih"

void p_forceExpr2Bool(SemVal *sv)
{
    if (svTestType(sv, e_bool))
        return;                             // done if boolean aloready 

    svPushStack(sv);                        // convert to code unless bool 

    svAddCode(sv, op_jmp_true);
    svAddCode(sv, op_jmp, j_falselist);

    svSetType(sv, e_stack | e_bool);
}
