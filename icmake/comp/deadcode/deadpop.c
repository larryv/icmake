#include "deadcode.ih"

void deadPop()                          // gp_idx: lastused, dead[0] = 0 
{
    if (dp_idx != 0)                    // anything to pop ? 
        --dp_idx;                       // then reduce the index
}
