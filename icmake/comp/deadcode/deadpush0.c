#include "deadcode.ih"

void deadPush0()                // next dead leavel, no dead code
{
    deadPush();
    dp_dead[dp_idx] = 0;
}
