#ifndef INCLUDED_STRINGSTORE_
#define INCLUDED_STRINGSTORE_

unsigned ssSize();
unsigned ssLength(unsigned index);
unsigned ssIndex(unsigned index);
char const *ssStr(unsigned index);

void ssSetBuf(char const *str);
void ssAppendBuf(char const *str);

char const *ssBuf();                        // scratch buffer's content

                                            // returns index in s_table of 
                                            // two concatenated strings 
unsigned ssCatenate(unsigned leftIdx, unsigned rightIdx);

                                            // returns strcmp of the strings
                                            // at leftIdx and rightIdx
int ssCompare(unsigned leftIdx, unsigned rightIdx);

unsigned ssFind(char const *str);           // returns the string's index in

#endif
