//#define msgx

#include "parser.ih"

void p_beginFunction()
{
                                // test if function name already exists 
    if (symtab_addFunction(gp_varType, ftell(gp_bin)) != 0)
    {
        util_semantic(sem_multiplyDefined, util_string());
        return;
    }
    
    msg("defining %s", util_string());

    symtab_push();              // setup the local symbol table 
                                // 1 global, 1 local symbol table 

    deadReset();                // allow code generation 

    ++gp_nestLevel;             // no code-writes to gp_bin in functions 

    msg("END");
}
