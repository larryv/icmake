#include "stringstore.ih"
                                            // returns index in s_table of 
                                            // the two catenated strings 
unsigned ssCatenate(unsigned leftIdx, unsigned rightIdx)
{
    register unsigned l_len = ssLength(leftIdx);
    register unsigned r_len = ssLength(rightIdx);

                                            // room for the catenated string 
    char *cat = rss_realloc(NULL, l_len + r_len + 1); 

    strcpy(cat, sp_table[leftIdx].str);     // catenate into cat
    strcpy(cat + l_len, sp_table[rightIdx].str); 

    unsigned ret = ssFind(cat);
    free(cat);

    return ret;
}
