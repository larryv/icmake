#include "int.ih"

void intCompare(Variable *lhs, Variable const *rhs)
{
    int lval = int_value(lhs);
    int rval = int_value(rhs);

    int_assignInt(lhs, lval < rval ? -1 : 
                       lval > rval ?  1 :
                                      0
                );
}

