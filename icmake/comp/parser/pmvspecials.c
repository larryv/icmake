//#define msgx

#include "parser.ih"

// see README.args for details

SemVal pmv_specials(ExprType type, SemVal *marg)  // array of arguments 
{
    if ((FunNr)type == f_execute)
        return pmv_execute(marg);                 // full list of arguments 

    SemVal ret = SVzero();
    register unsigned count = svValue(marg);

    *marg = smvArgs2code(marg);
    msg("Number of arguments: %d", svValue(marg));

    
    svPushArgs(&ret, marg);                     // push the args into ret

    svAddCode(&ret, op_push_imm, count);        // # of arguments of fun 
                                                // (note: this is the last
                                                //  argument that is pushed,
                                                //  just before calling the
                                                //  function.

    p_callRss(&ret, type, count + 1);           // call function 

    msg("END");

    return ret;
}

