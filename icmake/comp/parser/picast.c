#include "parser.ih"

void p_iCast(SemVal *sv)
{
    if (svTestType(sv, e_list))             // (int)list not ok 
        util_semantic(sem_illegalCast);
    else if (svTestType(sv, e_str))         // (int)string ok 
    {
        if (svTestType(sv, e_const))        // string const to a 
        {                                   // convert string to int
            svSetValue(sv, atoi(ssStr(svValue(sv))));
            svSetType(sv, e_const | e_int);
        }
        else
        {
            svPushStack(sv);                   // convert to code 
            svAddCode(sv, op_atoi);      // runtime conversion needed 
            svSetType(sv, e_int | e_stack);
        }
    }
}
