//#define msgx

#include "parser.ih"

SemVal pmv_return(ExprType op, SemVal *sv)
{
    msg("BEGIN 0x%x", sv->type);

    if (svTestType(sv, e_bool) || !svTestType(sv, e_stack))
        svPushStack(sv);

    msg("NEXT 0x%x", svType(sv));

    unsigned funIdx = symtab_lastFunction();

    if ((Opcode)op == op_ret)              // return opcode received 
    {
        ExprType funType = symtab_funType(funIdx);

        msg("Got op_ret, function type %s is 0x%x", symtab_funName(funIdx), \
                                                  funType);


                            // void if the union of the type of the function
                            // and the type of the expression is not a std
                            // type 
        if ((e_typeMask & (funType | svType(sv))) == 0)
        {
            msg("void function, generate 'ret' opcode");

            svAddCode(sv, op);    // generate the 'ret' opcode 

            msg("END");
            return SVmove(sv);    // done 
        }

        if (!(svTestType(sv, funType & e_typeMask)))
            util_semantic(sem_returnType, symtab_funName(funIdx));
    }

    msg("popping the pushed return value and returning");

    svAddCode(sv, op_pop_reg);
    svAddCode(sv, op);

    return SVmove(sv);
}
