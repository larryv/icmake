#include "semval.ih"

void svBool2int(SemVal *sv)
{
    if (!svTestType(sv, e_bool))            // no batchpatching needed 
        return;

    svPatchupTrue(sv, 1);
    sv->truelen = 0;

    svAddCode(sv, op_push_1_jmp_end);      // truelist target 

    svPatchupFalse(sv, 1);
    sv->falselen = 0;

    svAddCode(sv, op_push_0);              // falselist target

    svSetType(sv, e_int | e_stack);            /* set int code type */
}
