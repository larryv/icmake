#ifndef _INCLUDED_STACK_H_
#define _INCLUDED_STACK_H_

#include "../../rss/rss.h"

void        stack_push(Variable const *var);
void        stack_pushBP(void);

void        stack_pop(void);
void        stack_popBP(void);

Variable   *stack_top(void);            // the location of the last pushed
                                        // value. After 'push x; push 12'
                                        // stack_top() points to value 12,
                                        // and stack_top() - 1 points to x

Variable   *stack_local(int index);

#endif
