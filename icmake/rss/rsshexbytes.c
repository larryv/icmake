#include "rss.ih"

void rss_hexBytes(char const *hdr, int8_t const *str, unsigned length)
{
    fprintf(stderr, "%s: ", hdr);

    for (unsigned idx = 0; idx != length; ++idx)
        fprintf(stderr, "0x%x ", (unsigned const)(uint8_t const)str[idx]);

    fputc('\n', stderr);
}
