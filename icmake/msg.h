#ifndef INCLUDED_MSG_H_
#define INCLUDED_MSG_H_

// To activate msg(...) in classes do '#define msgx' in the class.ih files
// and recompile the class's source files
// or use separate #define msgx lines in source files

#ifdef msgx
    #define msg(...)    rss_msg_(__FILE__, __VA_ARGS__)
    #define svOut(x)    svShow(x)
#else
    #define msg(...)
    #define svOut(x)
#endif


#endif
