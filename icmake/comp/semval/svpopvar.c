//#define msgx

#include "semval.ih"

void svPopVar(SemVal *sv)
{
    s_lastOpcode = op_pop_var;

    if (!deadCode())
        sv->code[sv->codelen - sizeof(int16_t) - sizeof(int8_t)] = op_pop_var;
}

