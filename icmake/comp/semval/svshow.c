#include "semval.ih"

void svShow(SemVal const *sv)
{
    fprintf(stderr, "SemVal ");
    rss_showType(sv->type);
    fprintf(stderr, "       value:    %d\n"
                    "       codelen:  %d\n"
                    "       code:     %p\n"
                    "       truelen:  %d\n"
                    "       true:     %p\n"
                    "       falselen: %d\n"
                    "       false:    %p\n",
                    sv->value, 
                    sv->codelen, sv->code, 
                    sv->truelen, sv->truelist, 
                    sv->falselen, sv->falselist);

}

