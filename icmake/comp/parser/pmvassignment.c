//#define msgx
#include "parser.ih"
                                    // opstr is '=', or "/=", etc. 
                                    // destroys lval and rval
SemVal pmv_assignment(SemVal *lval, SemVal *rval, char const *opstr)
{
    unsigned type;
    unsigned value;

    msg("BEGIN rval len = %d", svCodeLength(rval));

    msg("lval type: %x", svType(lval));

    if (!svTestType(lval, e_var))
    {
        util_semantic(sem_lvalueNeeded, opstr);
        svDestroy(rval);
        return SVmove(lval);
    }

    p_tryString2IntConversion(lval, rval);

    //msg("rval after strin2int conversion:");
    //svOut(rval);

    svPushStack(rval);                      // convert rval to code 
    msg("rval len = %d, type = %x", svCodeLength(rval), svType(rval) );
    msg("rval after svPushStack");
    //svOut(rval);
                                            // same types 
    if (svType(lval) & svType(rval) & (e_int | e_str | e_list))
    {
        type = svType(lval);                // save type/idx for return
        value = svValue(lval);

        svAddCode(lval, op_copy_var, svValue(lval));
//FBB: REMOVE        gp_copyVar = 1;

        svCatenate(rval, lval);             // catenate pmv_assignment code 
        svSetType(rval, type);
        svSetValue(rval, value);

        msg("END: code len tmp: %d", svCodeLength(rval));
        return SVmove(rval);
    }

    util_semantic(sem_typeConflict, opstr);
    svDestroy(rval);

    msg("END");

    return SVmove(lval);
}


