//#define msgx

#include "semval.ih"

SemVal SVarg(SemVal *arg)
{
    SemVal ret = {0, };

    msg("argument type: %x", svType(arg));

    ret = smvAddArg(&ret, arg);

    msg("# arguments: %d", ret.value);

    for (int idx = 0; idx != ret.value; ++idx)
        msg("arg %d: type: %x", idx, svType(svArg(&ret, idx)));

    return ret;
}
