#include "parser.ih"

SemVal pmv_expressionAssign(SemVal *lval, SemVal *rval)
{
    SemVal tmp = pmv_assign(lval, rval);
    return pmv_expression(&tmp);
}
