/*
        This function converts the pushed strings into a list. The 1st
        stack argument specifies the #strings. The {\em reg} register 
        holds the list constant

    The stack is organized as follows:

        stack[0]        : # strings pushed before this value (at stack_top)
        stack[-1]       : the 1st string of the list
        stack[-2]       : the 2nd string of the list
        ...
        stack[-count]   : the last string of the list
*/

//#define msgx

#include "builtin.ih"

void b_listConst()
{
    msg("starting, at stack top: %d", int_value(stack_top()));

    listcons(eb_releaseReg());          // reg now holds a list

    for (int idx = 1, end = 1 + int_value(stack_top()); idx != end; ++idx)
    {
        msg("adding element %s", string_charp(stack_top() - idx));
        list_add_charPtr(&gb_reg, 
                            string_charp(stack_top() - idx));
    }

    msg("ending");
}









