#include "semval.ih"

SemVal SVtype(ExprType type, ...)
{
    va_list marker;
    SemVal ret = {type, };

    va_start(marker, type);

    switch ((int)type)
    {
        case e_int | e_const:
                                                // util_string());
            ret.value = atoi(va_arg(marker, char const *));
        break;

        case e_str | e_const:
                                                // p_findString index
            ret.value = ssFind(va_arg(marker, char const *));
        break;
    }

    return ret;
}
