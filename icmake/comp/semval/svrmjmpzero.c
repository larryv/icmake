#include "semval.ih"

void svRmJmpZero(SemVal *sv)
{
    if (sv->falselen && (sv->falselist[sv->falselen - 1] == sv->codelen))
        sv->codelen -= sizeof(int8_t) + sizeof(int16_t);

    svPatchupFalse(sv, 1);
}
