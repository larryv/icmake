#include "parser.ih"

    // mode is O_ALL, O_DIR, O_FILE, O_SUBDIR
    // expr is the pattern (mask, e.g. "*.cc")

SemVal pmv_makeList1(SemVal *regex, SemVal *mode)
{
    SemVal args = SVzero();
    args = smvAddArg(&args, mode);      // add the mode (e.g., IS_FILE)
    args = smvAddArg(&args, regex);     // add the regex

    return pmv_makeList(&args, op_hlt); // op_hlt: so not op_younger/op_older
}










