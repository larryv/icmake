#include "parser.ih"

void p_catStrings(SemVal *lval, SemVal const *rval)
{
    svSetValue(lval, ssCatenate(svValue(lval), svValue(rval)));
}
