#include "util.ih"

char *gu_sourceName;
char *gu_lexstring;

MsgCount gu_semantic[] =
{
    { ERRCOUNT, "function 'main()' not defined" },                  // sem_noMain,
    { ERRCOUNT, "`main(...)' must have 'void' return type" },       // sem_mainNotVoid,
    { ERRCOUNT, "%s multiply defined" },                            // sem_multiplyDefined,
    { ERRCOUNT, "Function `%s', argument %u: type mismatch" },      // sem_funArgTypeMismatch,
    { ERRCOUNT, "Function '%s' not defined" },                      // sem_funNotDefined
    { ERRCOUNT, "'break' only in 'while' or 'for' statements" },    // sem_noBreak
    { ERRCOUNT, "Function '%s()' requires %u arguments" },          // sem_argCount
    { ERRCOUNT, "'continue' only in 'while' or 'for' statements" }, // sem_noContinue,
    { ERRCOUNT, "division by 0: undefined" },                       // sem_zeroDiv,
    { ERRCOUNT, "%s undefined" },                                   // sem_undefined,
    { ERRCOUNT, "modulo 0: undefined" },                            // sem_noModulo0,
    { ERRCOUNT, "Incorrect returntype for function '%s()'" },       // sem_returnType,
    { ERRCOUNT, "conflicting operand types for %s" },               // sem_typeConflict,   
    { ERRCOUNT, "illegal type for %s" },                            // sem_illegalType,    
    { ERRCOUNT, "lvalue needed for %s" },                           // sem_lvalueNeeded,   
    { ERRCOUNT, "%s(): too few arguments" },                        // sem_illegalArgCount,
    { ERRCOUNT, "illegal cast" },                                   // sem_illegalCast,    
    { ERRCOUNT, "[...] requires string argument(s)" },              // sem_stringsRequired,
};

int gu_showSemantic = 1;



