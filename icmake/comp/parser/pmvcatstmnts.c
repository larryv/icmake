//#define msgx

#include "parser.ih"

SemVal pmv_catStmnts(SemVal *lval, SemVal *rval)
{
    msg("BEGIN");

    svPatchupFalse(lval, 1);

    msg("lval length: %u, rval length: %u", lval->codelen, rval->codelen);

    if (gp_nestLevel == 0)
    {
        util_out(gp_bin, lval->code, lval->codelen);
        svDestroy(lval);
        return SVmove(rval);
    }

    svCatenate(lval, rval);

    msg("END");

    return SVmove(lval);
}
