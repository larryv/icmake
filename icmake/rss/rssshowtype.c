#include "rss.ih"

static char const *typeName[] = 
{
    /* rss + compiler: */
    "e_int",              /* 1: int-type    expression */
    "e_str",              /* 2: string-type expression */
    "e_list",             /* 4: list-type   expression */

    /* compiler only: */
    "e_bool",             /* 8: bool-type   expression */


    "e_const",            /* 0x10 immediate value */
    "e_var",              /* 0x20 variable */
    "e_reg",              /* 0x40 register */
    "e_stack",            /* 0x80 value available on the stack */

    "e_pre_inc_dec",      /* 0x100 pre-inc or pre-dec        */
    "e_post_inc_dec",     /* 0x200 post-inc or post-dec      */
};

void rss_showType(unsigned type)
{
    fprintf(stderr, "type:    %x:", type);
    if (type == e_null)
    {
        fputs(" e_null\n", stderr);
        return;
    }

    for (int idx = 0, mask = 1; idx != 10; ++idx, mask <<= 1)
    {
        if ((type & mask) != 0)
            fprintf(stderr, " %s", typeName[idx]);
    }

    fputc('\n', stderr);
}
