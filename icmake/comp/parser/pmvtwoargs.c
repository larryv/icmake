//#define msgx
#include "parser.ih"

SemVal pmv_twoArgs(ExprType type, SemVal *larg, SemVal *rarg)
{
    register int ok;

    msg("BEGIN");

    svPushStack(larg);                             // arg to stack 
    svPushStack(rarg);                             // arg to stack 

    ok = p_twoArgsFunTest(&type, larg, rarg);

    msg("types test %d, funstring: %x", ok, type);

    if (ok)
    {
        svCatenate(rarg, larg);            // make one code vector 
        p_callRss(rarg, type);
    }
    else
    {
        util_semantic(sem_typeConflict, gp_funstring[type]);
        *rarg = pmv_semVal(2, larg, rarg);
    }

    msg("END");
    return SVmove(rarg);
}





