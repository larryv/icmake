//#define msgx

#include "semval.ih"

// see ../parser/README.args for details

SemVal smvAddArg(SemVal *sv, SemVal *arg)
{
    msg("argument type: %x", svType(arg));
    svPushStack(arg);                      // convert arg to code

    msg("argument type: %x", svType(arg));

    unsigned codelen = sv->codelen;

    ++sv->value;                            // update the args count
    msg("# arguments out: %d", sv->value);

                                            // room for *arg
    sv->code = rss_realloc(sv->code, codelen + sizeof(SemVal));

    *(SemVal *)&sv->code[codelen] = SVmove(arg);  // move arg into
                                                        // position 

    sv->codelen += sizeof(SemVal);          // update the codelen

    *arg = SVzero();                      // arg grabbed

    return SVmove(sv);
}
