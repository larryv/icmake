#include "parser.ih"

SemVal pmv_and(SemVal *lexp, SemVal *rexp)
{
    if (svType(lexp) & svType(rexp) & e_const)  // and-result of constants
    {
        svSetValue(lexp, 
            (svTestType(lexp, e_str) || svValue(lexp))
            &&
            (svTestType(rexp, e_str) || svValue(rexp))
        );
        svSetType(lexp, e_const | e_int);
    }
    else                                    // at least one code-part 
    {
        p_forceExpr2Bool(lexp);
        p_forceExpr2Bool(rexp);

        svPatchupTrue(lexp, 1);

        svCatenate(lexp, rexp);
        svSetType(lexp, e_bool | e_stack);
    }

    return SVmove(lexp);
}
