//#define msgx

#include "semval.ih"

SemVal SVmove(SemVal *src)
{
    SemVal ret =  *src;
    *src = SVzero();

    return ret;
}
