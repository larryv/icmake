#include "parser.ih"

void parser(char **argv)
{
    symtab();
    scanner(argv[1]);

    if (!(gp_bin = fopen(argv[2], "w+b")))   // open binary file  
        rss_fatal(0, 0, "%s Can't write `%s'", argv[2]);

    deadInit();                             // initialize the dead-stack

    ssSetBuf("");                           // initialize the StringStore 
                                            // scratch string

    util_setString("");                     // initial util_string() 

                                            // go to first codebyte pos 
    fseek(gp_bin, sizeof(BinHeader), SEEK_SET);


    #if YYDEBUG
        yydebug = YYDEBUG;
    #endif
}


