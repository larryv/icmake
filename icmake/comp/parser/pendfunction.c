//#define msgx
#include "parser.ih"

static int8_t opret = op_ret;

void p_endFunction(SemVal *funStmnt)
{
    msg("START");

    gp_nestLevel = 0;       // function completed: OK to write code to gp_bin 

    p_makeFrame();          // make the frame, defining the local variables 

    p_lastStmnt(funStmnt);  // add the function's statements, patching 
                            // funStmnnt's false list 

    if (! deadCode())
        util_out(gp_bin, &opret, sizeof(int8_t)); // add a 'ret' instruction 
    else
        deadZero();         // leaving a function: code generation ok,  
                            // e.g. to define global variables          

    symtab_cleanup();        // pop all but the global symtab, update the local
                             // variable offsets 

    svDestroy(funStmnt);

    util_resetSemErr();

    for (unsigned idx = 0; idx != gp_nErrMsgs; ++idx)
        gp_errMsg[idx].count = ERRCOUNT;

    msg("END");
}







