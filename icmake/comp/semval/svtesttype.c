//#define msgx

#include "semval.ih"

unsigned svTestType(SemVal const *sv, unsigned oredExprTypeValues)
{
    return sv->type & oredExprTypeValues;
}

