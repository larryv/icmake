#include "parser.ih"

int p_testOperand(SemVal const *sv, Opcode opcode)
{
                                                // 0 means  no type error
    register int ret = !svTestType(sv, gp_opType[opcode]);  

    if (ret)
        util_semantic(sem_illegalType, gp_opstring[opcode]);

    return ret;
}
