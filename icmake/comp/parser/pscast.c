#include "parser.ih"

void p_sCast(SemVal *sv)
{
    char buffer[10];

    if (svTestType(sv, e_list))               // (string)list not ok 
        util_semantic(sem_illegalCast);
    else if (svTestType(sv, e_int))           // (string)int ok 
    {
        if (svTestType(sv, e_const))
        {
            sprintf(buffer, "%u", 
                  (unsigned)svValue(sv));     // convert to string 

            svSetValue(sv, ssFind(buffer));
            svSetType(sv, e_const | e_str);
        }
        else
        {
            svPushStack(sv);                        // convert to code 
            svAddCode(sv, op_itoa);
            svSetType(sv, e_stack | e_str);
        }
    }
}
