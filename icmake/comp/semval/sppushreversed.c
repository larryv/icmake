//#define msgx

#include "semval.ih"

    // arg points at the first SemVal in a SemVal argument structure
    // arg[nArgs - 1] is pushed first, arg is pushed last.
void sp_pushReversed(int8_t *dest, SemVal *arg, unsigned nArgs)
{
    msg("BEGIN: nArgs to push: %d", nArgs);

    arg += nArgs;

    for (; nArgs--; )                       // visit nArgs arguments
    {                                       // last to first
        --arg;                              // SemVal to copy code from 

        msg("arg: idx = %d, code len = %d, dest = %p", nArgs, arg->codelen,\
                                                                dest);

        memcpy(dest, arg->code, arg->codelen);  // copy the code

        dest += arg->codelen;               // next dest is beyond the just
                                            // copied code

        svDestroy(arg);                     // destroy the copied argument
    }

    msg("END: returned code len = %d", arg->codelen);
}
