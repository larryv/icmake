#include "parser.ih"

SemVal pmv_compl(SemVal *sv)
{
    if (p_testOperand(sv, op_bnot))            // test types ok 
    {
        util_semantic(sem_illegalType, gp_opstring[op_bnot]);
        return SVmove(sv);
    }

    if (svTestType(sv, (unsigned)~e_typeMask) == e_const)   // immediate value 
        svSetValue(sv, ~svValue(sv));
    else
    {
        svPushStack(sv);                        // convert to code 
        svAddCode(sv, op_bnot);                 // generate instruction 
    }

    return SVmove(sv);
}
