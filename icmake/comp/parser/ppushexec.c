#include "parser.ih"

void p_pushExec(SemVal *ret, SemVal *args, unsigned idx, Opcode function)
{
    svCatenate(ret, &args[idx]);
    p_callRss(ret, function);
}
