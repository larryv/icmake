//#define msgx
#include "parser.ih"

SemVal pmv_if(SemVal *cond, SemVal *trueStmnt, SemVal *falseStmnt)
{
    unsigned len;
    unsigned *list;

    msg("BEGIN");

    --gp_nestLevel;                     // reduce nesting level 

    p_expr2bool(cond);                  // make links for cond 

    if (svTestType(cond, e_const))       // consts: either always/never 
    {
        int value = svValue(cond);
        svDestroy(cond);
        if (value)                      // trueStmnt always executed 
        {
            svDestroy(falseStmnt);
            return  SVmove(trueStmnt);
        }
                             
        svDestroy(trueStmnt);           // falseStmnt always executed 
        return SVmove(falseStmnt);
    }

    svPatchupTrue(cond, 1);              // true dest: the end of cond 

    if (svType(falseStmnt) == e_null)       // no falsestmnt 
    {
        svCatenate(cond, trueStmnt); // cond = cond ~ trueStmnt 
        return SVmove(cond);
    }

    svAddCode(trueStmnt, op_jmp, j_falselist);

    svGrabFalse(&list, &len, trueStmnt);

    p_addPatch(list, len, svCodeLength(cond)); // increase the patch targets 

    svCatenate(cond, trueStmnt);            // cond = cond ~ trueStmnt 
    svPatchupFalse(cond, 1);                // patch to EOC 

    svSetFalse(cond, list, len);            // install the falselist at cond

    svCatenate(cond, falseStmnt);           // if .. else cat. and return
    return SVmove(cond);
}




