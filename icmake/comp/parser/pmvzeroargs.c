#include "parser.ih"

SemVal pmv_zeroArgs(ExprType type)
{
    SemVal ret = SVzero();
    p_callRss(&ret, type);

    return ret;
}
