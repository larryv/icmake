#include "parser.ih"

SemVal pmv_multiply(SemVal *lval, SemVal *rval)
{
    if (p_testBinOp(op_mul, lval, rval))
        return pmv_semVal(2, lval, rval);     // test for correct types 

    svBool2int(lval);                       // convert pending booleans 
    svBool2int(rval);

    if (p_conflict(lval, rval, op_mul))     // test type p_conflict 
        return pmv_semVal(2, lval, rval);    

    if ( (svType(lval) & svType(rval) & (unsigned)~e_typeMask)  == e_const)
        svSetValue(lval, svValue(lval) * svValue(rval));
    else
        *lval = pmv_binOp(lval, rval, op_mul);

    return SVmove(lval);              // return new expression 
}
