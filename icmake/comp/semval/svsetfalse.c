//#define msgx

#include "semval.ih"

void svSetFalse(SemVal *sv, unsigned *list, unsigned len)
{
    sv->falselist = list;
    sv->falselen = len;
}
