#include "parser.ih"

void p_lCast(SemVal *sv)
{
    if (svTestType(sv, e_int))               // (int)list not ok 
        util_semantic(sem_illegalCast);
    else if (svTestType(sv, e_str))         // (string)int ok 
    {
        svPushStack(sv);                    // convert to code 
        svAddCode(sv, op_atol);
        svSetType(sv, e_stack | e_list);
    }
}
