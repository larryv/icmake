//#define msgx

#include "semval.ih"

int svValue(SemVal const *sv)
{
    msg("BEGIN");
    svOut(sv);

    return sv->value;
}

