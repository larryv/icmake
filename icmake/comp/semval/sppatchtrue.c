#include "semval.ih"

void sp_patchTrue(SemVal *sv)
{                                           // expand the truelist 
    sv->truelist = rss_realloc(sv->truelist, 
                               (sv->truelen + 1) * sizeof(unsigned));

                                            // room for the jump-backpatch 
    sv->code = rss_realloc(sv->code, sv->codelen += sizeof(int16_t));

    sv->truelist[sv->truelen++] = sv->codelen;   // store jumpstart location 
}
