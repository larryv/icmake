//#define msgx
#include "parser.ih"

SemVal pmv_incDec(PREPOST_ pp, Opcode opcode, SemVal *sv)
{
    register unsigned varnr;

    if (p_testOperand(sv, opcode))
    {
        util_semantic(sem_illegalType, gp_opstring[opcode]);
        return SVmove(sv);
    }

    if (!svTestType(sv, e_var))
    {
        util_semantic(sem_lvalueNeeded, gp_opstring[opcode]);
        return SVmove(sv);
    }

    msg("BEGIN");
    //svOut(sv);

    varnr = svValue(sv);

    msg("   add inc/dec opcode %x, length = %d", opcode, svCodeLength(sv));
    svAddCode(sv, opcode, varnr);         // Generate INC/DEC opcode 
    msg("   added inc/dec opcode length = %d", svCodeLength(sv));

    svSetType(sv,                           // Indicate pre/post inc- decrement 
            pp == pre_op ?
                e_int | e_pre_inc_dec       // NB: no e_stack yet: push the  
            :                               //     value on the stack when  
                e_int | e_post_inc_dec      //     required                 
    );

    msg("END: pp == pre_op: %d, preincdec: %x, postincdec: %x",\
        pp == pre_op, e_int | e_pre_inc_dec, e_int | e_post_inc_dec );
    svOut(sv);

    //rss_hexBytes("INCDEC: ", sv->code, sv->codelen);

    return SVmove(sv);
}
