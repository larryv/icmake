#include "parser.ih"

SemVal pmv_modulo(SemVal *lval, SemVal *rval)
{
    if (p_testBinOp(op_mod, lval, rval))
        return pmv_semVal(2, lval, rval);     // test for correct types 

    svBool2int(lval);                       // convert pending booleans 
    svBool2int(rval);

    if (p_conflict(lval, rval, op_mod))     // test type p_conflict 
        return pmv_semVal(2, lval, rval);

    if (svTestType(rval, e_const))
    {
        if (!svValue(rval))                   // no "E / 0" 
        {
            util_semantic(sem_noModulo0);
            return pmv_semVal(2, lval, rval);
        }

        if (svTestType(lval, e_const))
        {
            svSetValue(lval, svValue(lval) % svValue(rval));
            return SVmove(lval);
        }
    }

    return pmv_binOp(lval, rval, op_mod);   // return new expression 
}





