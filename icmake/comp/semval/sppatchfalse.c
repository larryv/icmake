#include "semval.ih"

void sp_patchFalse(SemVal *sv)
{
    sv->falselist = rss_realloc(sv->falselist,   // expand the falselist 
                            (sv->falselen + 1) * sizeof(unsigned));

                                            // room for the jump-backpatch 
    sv->code = rss_realloc(sv->code, sv->codelen += sizeof(int16_t));
    sv->falselist[sv->falselen++] = sv->codelen;   // store jumpstart location 
}
