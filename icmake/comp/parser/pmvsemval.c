#include "parser.ih"

SemVal pmv_semVal(int count, ...)
{
    va_list list;
    
    va_start(list, count);

    for (; count--; )
        svDestroy(va_arg(list, SemVal *));

    return SVzero();
}
