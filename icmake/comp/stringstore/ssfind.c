//  Lookup (and maybe enter) a string

#include "stringstore.ih"

static unsigned ssCapacity;

unsigned ssFind(char const *str)        // returns the string's index in
{                                       //  sp_table
    register unsigned idx;

    for (idx = 0; idx != sp_size; ++idx)
        if (strcmp(sp_table[idx].str, str) == 0)    // string found ? 
            return idx;                             // return its idx 

    if (sp_size == ssCapacity)                      // table is filled up?
        sp_table = rss_realloc(sp_table,            // make room for more
                             (ssCapacity += 20) * sizeof(Element));

                                                // set the string in memory 
    sp_table[sp_size].str = rss_strdup(str);
    sp_table[sp_size].sectionIndex = sp_stringSectionSize;

    sp_stringSectionSize += strlen(str) + 1;

    ++sp_size;                                  // available location

    return idx;                                 // return the string's index
                                                // in the string table
}



