#include "parser.ih"

SemVal pmv_not(SemVal *sv)
{
    if (svTestType(sv, e_const))                // immediate value 
    {
        svSetValue(sv, !(svTestType(sv, e_str) || svValue(sv)));
        svSetType(sv, e_int | e_const);
    }
    else
    {
        p_forceExpr2Bool(sv);
        svSwapTrueFalse(sv);
    }

    return SVmove(sv);
}
