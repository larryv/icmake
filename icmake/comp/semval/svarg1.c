//#define msgx

#include "semval.ih"

SemVal const *svArg(SemVal const *sv, unsigned idx)
{
    return ((SemVal const *)sv->code) + idx;
}
