//#define msgx
#include "semval.ih"

SemVal SVint(int value)
{
    SemVal ret = {e_int | e_const, value };

    msg("mode type: %x, value: %d", ret.type, ret.value);

    return ret;
}
