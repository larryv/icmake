//#define msgx
#include "parser.ih"

int p_checkArgumentTypes(unsigned nParams, unsigned funIdx, 
                                           SemVal const *args)
{
    register unsigned idx;
    int ret = 1;                            // assume no type errors

    msg("checking %d parameters", nParams);

    for (idx = 0; idx != nParams; ++idx, ++args)
    {
        int paramType = symtab_funParameterType(funIdx, idx);
        int argType = svType(args);

        msg("arg %d: type = %x, expect param type = %x", idx,\
                                                    argType, paramType);

        if (paramType != (argType & e_typeMask))
        {
            util_semantic(sem_funArgTypeMismatch,
                symtab_funName(funIdx), idx + 1);
            ret = 0;                        // type error encountered
        }
    }

    msg("END");
    return ret;
}

