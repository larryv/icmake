#include "parser.ih"

SemVal pmv_assign(SemVal *lval, SemVal *rval)     // destroys lval, rval
{
    return pmv_assignment(lval, rval, "=");
}
