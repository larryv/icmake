#include "parser.ih"

SemVal pmv_compoundAss(SemVal *lval, SemVal *rval, 
                     SemVal (*operator)(SemVal *, SemVal *),
                     char const *compoundOperator)
{
    register ExprType ltype = svType(lval); // save the variable's type and
    register unsigned value = svValue(lval);// index value

    *lval = operator(lval, rval);           // perform operation: lval 
                                            // contains the resulting value

    *rval = SVint(value);
    svSetType(rval, ltype);                   // restore ltype/value 

    return pmv_assignment(rval, lval, compoundOperator);  // perform assignment 
}
