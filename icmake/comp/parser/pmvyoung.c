//#define msgx
#include "parser.ih"

SemVal pmv_young(SemVal *lval, SemVal *rval)
{
    msg("start");

    if (p_testBinOp(op_younger, lval, rval))
        return pmv_semVal(2, lval, rval);     // test for correct types 

    svPushStack(lval);                     // convert to code 
    svPushStack(rval);

    return pmv_binOp(lval, rval, op_younger);
}
