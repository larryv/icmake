#include "parser.ih"

SemVal pmv_old(SemVal *lval, SemVal *rval)
{
    if (p_testBinOp(op_older, lval, rval))
        return pmv_semVal(2, lval, rval);     // test for correct types 

    svPushStack(lval);                     // convert to code 
    svPushStack(rval);

    return pmv_binOp(lval, rval, op_older);
}


