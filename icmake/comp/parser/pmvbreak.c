#include "parser.ih"

SemVal pmv_break()
{
    SemVal ret = SVzero();

    if (!gp_breakOK)
        util_semantic(sem_noBreak);
    else
    {
        svAddCode(&ret, op_jmp, j_truelist);
        svSetType(&ret, e_bool | e_stack);
        deadInc();                    // next code is dead 
    }

    return ret;
}
