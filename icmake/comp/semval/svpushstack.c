//#define msgx

// change the code so that an expression value is pushed on the run-time stack
#include "semval.ih"

void svPushStack(SemVal *sv)
{
    msg("BEGIN");
    //svOut(sv);

    switch
    (
        svTestType(sv, e_const | e_var | e_bool | e_reg  |
                       e_pre_inc_dec | e_post_inc_dec)
    )
    {
        case e_const:
            svAddCode(sv,
                svTestType(sv, e_int) ? op_push_imm : op_push_strconst,
                sv->value
            );
            msg("ADD CODE code len: %d", sv->codelen);
        break;

        case e_var:
            msg("e_var");
            if (!sv->codelen)
                svAddCode(sv, op_push_var, sv->value);
        break;

        case e_bool:
            msg("e_bool");
            svPatchupTrue(sv, 1);
            svAddCode(sv, op_push_1_jmp_end);
            svPatchupFalse(sv, 1);
            svAddCode(sv, op_push_0);
            svSetType(sv, e_int);
        break;

        case e_reg:
            msg("e_reg");
            svAddCode(sv, op_push_reg);
        break;

        case e_post_inc_dec:
        {
            msg("e_post_inc_dec");
            SemVal pre = SVzero();
            pre.type = e_int;

            svAddCode(&pre, op_push_var, sv->value);
            svCatenate(&pre, sv);     // prefix push before var++, var-- 
            *sv = SVmove(&pre);         // sv holds the code
        }
        break;

        case e_pre_inc_dec:                 // append push after ++var, --var
            msg("e_pre_inc_dec");
            svAddCode(sv, op_push_var, sv->value);
        break;
    }

    msg("type in: %x", svType(sv));
    
    sp_downType(sv, e_const | e_var | e_bool | e_reg |
                    e_pre_inc_dec | e_post_inc_dec);

    msg("type down: %x", svType(sv));

    sp_upType(sv, e_stack);

    msg("type up: %x", svType(sv));

    msg("END\n");
}





