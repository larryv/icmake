//#define msgx
#include "semval.ih"

void svCatenate(SemVal *e1, SemVal *e2)
{
    register unsigned e1codeLen = e1->codelen;
    register unsigned e2codeLen = e2->codelen;

    msg("lval len = %d, rval len = %d", svCodeLength(e1), \
                                        svCodeLength(e2));

    e1->code = rss_realloc(e1->code,       // room for new code 
                          (e1codeLen + e2codeLen) * sizeof(int8_t));

                                            // catenate the code 
    memcpy(e1->code + e1codeLen, e2->code, e2codeLen * sizeof(int8_t));
    e1->codelen += e2codeLen;                     // new size 

    sp_patchAdd(e1codeLen, &e1->truelist, &e1->truelen,
                e2->truelist, e2->truelen);

    sp_patchAdd(e1codeLen, &e1->falselist, &e1->falselen,
                e2->falselist, e2->falselen);

    sp_patchAdd(e1codeLen, &e1->continuelist, &e1->continuelen,
                e2->continuelist,  e2->continuelen);

    e1->type |= e2->type;               // type of combined code 
                                        // (is ok with same types) 

    svDestroy(e2);                      // new semval in *e1
    
    msg("post lval len = %d", svCodeLength(e1));
}




