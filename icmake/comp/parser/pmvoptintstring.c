#include "parser.ih"

SemVal pmv_optIntString(ExprType type, SemVal *larg, SemVal *rarg)
{
    svPushStack(larg);                             // argument to stack 

    if (
        svTestType(larg, e_str)                 // left is string 
        &&
        svType(rarg) == e_null                  // right is zeroframe 
    )                                           // -> REQUIRE_ZERO implied 
    {
        svSetType(rarg, e_int | e_const);       // indicate constant 
        svSetValue(rarg, REQUIRE_ZERO);         // indicate REQUIRE_ZERO  
        svPushStack(rarg);                      // right arg now code 
        svCatenate(larg, rarg);                 // catenate code 
    }
    else if (
        svTestType(larg, e_int)                 // left is int: explicit check 
        &&
        svTestType(rarg, e_str)                 // right must be string 
    )
    {
        svPushStack(rarg);                      // right arg to code 
        svCatenate(rarg, larg);
        *larg = SVmove(rarg);
    }
    else
    {
        util_semantic(sem_typeConflict, gp_funstring[type]);
        svDestroy(rarg);
        return SVmove(larg);
    }

    p_callRss(larg, type);                      // call the function 
    return SVmove(larg);
}
