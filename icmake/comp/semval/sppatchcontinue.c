#include "semval.ih"

void sp_patchContinue(SemVal *sv)
{
                                            // expand the continuelist 
    sv->continuelist = rss_realloc(sv->continuelist,
                            (sv->continuelen + 1) * sizeof(unsigned));

                                            // room for the jump-backpatch 
    sv->code = rss_realloc(sv->code, sv->codelen += sizeof(int16_t));
                                            // store jumpstart location 

    sv->continuelist[sv->continuelen++] = sv->codelen;   
}
