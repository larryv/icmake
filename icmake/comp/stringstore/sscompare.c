
#include "stringstore.ih"

int ssCompare(unsigned leftIdx, unsigned rightIdx)
{
    return strcmp(sp_table[leftIdx].str, sp_table[rightIdx].str);
}
