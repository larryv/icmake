#include "parser.ih"

int p_allStrings(SemVal const *args)        // SemVal argument structure
{
    for (int idx = 0, end = svValue(args); idx != end; ++idx)
    {
        if ((svType(svArg(args, idx)) & e_str) == 0)    // not a string?
        {
            util_semantic(sem_stringsRequired);
            return 0;
        }
    }
    return 1;
}
