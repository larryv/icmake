//#define msgx

#include "semval.ih"

    // args holds a SemVal argument structure.
void svPushArgsSubset(SemVal *sv, SemVal *args, unsigned begin, 
                                                unsigned count)
{
    msg("BEGIN: first arg idx: %d, #args to push: %d", begin, count);

    unsigned codelen = sv->codelen + sp_argsCodelen(args, begin, count);

    msg("old code length: %d, new code length: %d", sv->codelen, codelen);

    sv->code = rss_realloc(sv->code, codelen);  // extend the code space
    
    msg("code starts at %p", sv->code);

                                                // push the args
    sp_pushReversed(sv->code + sv->codelen, 
                    (SemVal *)svArg(args, begin), count);

    sv->codelen = codelen;

    msg("END");
}
