//#define msgx

#include "parser.ih"

    // only called from pmv_execute: see that function for details
    // this function sets the cmd/arg heads and tails

SemVal pmv_setHeadsTails(SemVal *args, unsigned count)
{
    msg("BEGIN");

    SemVal ret = SVzero();

    p_pushExec(&ret, args, 2, f_cmd_head);
    p_pushExec(&ret, args, 3, f_arg_head);
    p_pushExec(&ret, args, count - 2, f_arg_tail);
    p_pushExec(&ret, args, count - 1, f_cmd_tail);

    msg("Erased args [2], [3], [%d], [%d] ", count - 2, count - 1);

    msg("END. code len: %d", svCodeLength(&ret));

    return ret;
}
