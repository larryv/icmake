//#define msgx

#include "parser.ih"

SemVal pmv_notEqual(SemVal *lval, SemVal *rval)
{
    msg("BEGIN: lval: %d, rval: %d", svValue(lval), svValue(rval));

    svOut(lval);
    svOut(rval);

    svBool2int(lval);                           // convert boolean to i 
    svBool2int(rval);

    msg("CONVERTED: lval: %d, rval: %d", svValue(lval), svValue(rval));
    svOut(lval);
    svOut(rval);

    if (p_conflict(lval, rval, op_neq))         // test type p_conflict 
        return pmv_semVal(2, lval, rval);

                                            // not both constant values
    if ((svType(lval) & svType(rval) & (unsigned)~e_typeMask) != e_const)
        *lval = pmv_binOp(lval, rval, op_neq);
    else if (svTestType(lval, e_int))       // or: lval is an int
        svSetValue(lval, svValue(lval) != svValue(rval));
    else                                    // or: return an int const
    {
        svSetType(lval, e_int | e_const);
        svSetValue(lval, ssCompare(svValue(lval), svValue(rval)) != 0);
    }

    msg("RETURNEDED:");
    svOut(lval);
    msg("END");

    return SVmove(lval);
}





