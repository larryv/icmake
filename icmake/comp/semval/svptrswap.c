//#define msgx

#include "semval.ih"

void svPtrSwap(SemVal **one, SemVal **two)
{
    SemVal *tmp = *one;
    *one = *two;
    *two = tmp;
}
