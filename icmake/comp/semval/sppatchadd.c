//#define msgx

#include "semval.ih"

void sp_patchAdd(register unsigned value, unsigned **dest, 
                 unsigned *dlen, unsigned *source, unsigned slen)
{
    register unsigned idx;

    if (!*dlen)
        *dest = NULL;                       // no memory for dest as yet 

    for (idx = 0; idx < slen; idx++)        // all elements of source list: 
        source[idx] += value;               // icrement addresses of targets 

                                            // expand the dest area 
    *dest = rss_realloc(*dest, (*dlen + slen) * sizeof(unsigned));

                                            // append source list 
    memcpy(*dest + *dlen, source, slen * sizeof(unsigned));
    *dlen += slen;                          // increment # element 
}

