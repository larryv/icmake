#include "icmun.ih"

BinHeader *headerp;

char
    *funname [] =
    {
        /* 0 */
        "arghead",
        "argtail",
        "ascii_int2str",
        "ascii_str2int",

        /* 4 */
        "backtick",
        "change_base",
        "change_ext",
        "change_path",

        /* 8 */
        "chdir",
        "cmdhead",
        "cmdtail",
        "echo",

        /* c */
        "list_element",
        "eval",
        "exec",
        "execute",

        /* 10 */
        "exists",
        "fgets",
        "fprintf",
        "get_base",

        /* 14 */
        "get_dext",
        "get_ext",
        "get_path",
        "getch",

        /* 18 */
        "getenv",
        "getpid",
        "gets",
        "listlen",

        /* 1c */
        "makelist",
        "printf",
        "putenv",
        "stat",

        /* 20 */
        "string_element",
        "strfind",
        "strformat",
        "strlen",

        /* 24 */
        "strlwr",
        "resize",
        "strtok",
        "strupr",

        /* 28 */
        "substr",
        "system",
        "trim",
        "trimleft",

        /* 2c */
        "trimright",
        "strchr",
        "listfind",
        "listunion",

        /* 30 */
        "listConst",
    };

FILE *infile;

int8_t *local_types;

unsigned
    curoffs,
    nvar;

Variable *var;

void
    (*p_procfun[])(void) =              // see rss/types.ih
    {
                                    
        fun_jmp,                    /*     00     */
        fun_jmp_false,              /*     01     */
        fun_jmp_true,               /*     02     */
        fun_push_1_jmp_end,         /*     03     */
        fun_push_0,                 /*     04     */
        fun_push_imm,               /*     05     */
        fun_push_strconst,          /*     06     */
        fun_push_var,               /*     07     */
        fun_push_reg,               /*     08     */
        fun_pop_var,                /*     09     */
        fun_umin,                   /*     0a     */
        fun_atoi,                   /*     0b     */
        fun_itoa,                   /*     0c     */
        fun_atol,                   /*     0d     */
        fun_mul,                    /*     0e     */
        fun_div,                    /*     0f     */
        fun_mod,                    /*     10     */
        fun_add,                    /*     11     */
        fun_sub,                    /*     12     */
        fun_eq,                     /*     13     */
        fun_neq,                    /*     14     */
        fun_sm,                     /*     15     */
        fun_gr,                     /*     16     */
        fun_younger,                /*     17     */
        fun_older,                  /*     18     */
        fun_smeq,                   /*     19     */
        fun_greq,                   /*     1a     */
        fun_call_rss,               /*     1b     */
        fun_asp,                    /*     1c     */
        fun_exit,                   /*     1d     */
        fun_copy_var,               /*     1e     */
        fun_inc,                    /*     1f     */
        fun_dec,                    /*     20     */
        fun_call,                   /*     21     */
        fun_frame,                  /*     22     */
        fun_ret,                    /*     23     */
        fun_pop_reg,                /*     24     */
        fun_band,                   /*     25     */
        fun_bor,                    /*     26     */
        fun_bnot,                   /*     27     */
        fun_xor,                    /*     28     */
        fun_shl,                    /*     29     */
        fun_shr,                    /*     2a     */

        /* fun_hlt : bogus value... op_hlt does not really exist */
    };
