#include "parser.ih"

        // args refers tp a SemVal argument structure
        // (already converted to code)
SemVal pmv_fprintf(ExprType type, SemVal *args)
{
    register int ok;
    SemVal const *arg;

    if (svValue(args) < 2)                   // argcount must be at least 2 
    {
        util_semantic(sem_illegalArgCount, gp_funstring[type]);
        return SVmove(args);
    }

    arg = svArg(args, 0);                    // pointer to first arg 

    switch ((FunNr)type)
    {
        case f_fprintf:                     // first arg must be string 
            ok = svTestType(arg, e_str);
        break;

        default:                            // 1st arg must be int,   
            ok = svTestType(arg, e_int)     // 2nd arg must be string 
                 &&
                 svTestType(svArg(args, 1), e_str);
    }

    if (ok)
        return pmv_specials(type, args);      // return pmv_specials call 

                                            // type p_conflict error 
    util_semantic(sem_typeConflict, gp_funstring[type]); 
    return SVmove(args);
}
