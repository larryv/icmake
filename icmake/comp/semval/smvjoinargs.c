//#define msgx

#include "semval.ih"

// see ../parser/README.args for details

SemVal smvJoinArgs(SemVal *args1, SemVal *args2)
{
    unsigned newLen = args1->codelen + args2->codelen;

    args1->value += args2->value;           // update the args count
                                                
    args1->code = rss_realloc(args1->code, newLen); // room for all args

                                            // append arg2's SemVals
    memcpy(args1->code + args1->codelen, args2->code, args2->codelen);
    *args2 = SVzero();                      // grab args2

    args1->codelen = newLen;                // update the codelen

    return SVmove(args1);             // move args1
}
