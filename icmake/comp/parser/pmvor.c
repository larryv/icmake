#include "parser.ih"

SemVal pmv_or(SemVal *lexp, SemVal *rexp)
{
    if (svType(lexp) & svType(rexp) & e_const)  // or-result of constants
    {
        svSetValue(lexp, 
            (svTestType(lexp, e_str) || svValue(lexp))
            ||
            (svTestType(rexp, e_str) || svValue(rexp))
        );
        svSetType(lexp, e_const | e_int);
    }
    else                                    // at least one code-part 
    {
        p_forceExpr2Bool(lexp);                        // boolean code 
        p_forceExpr2Bool(rexp);

        svRmJmpZero(lexp);

        svCatenate(lexp, rexp);
        svSetType(lexp, e_bool | e_stack);
    }

    return SVmove(lexp);
}


