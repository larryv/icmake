//#define msgx

#include "semval.ih"

    // args holds a SemVal argument structure.
void svPushArgs(SemVal *sv, SemVal *args)
{
    svPushArgsSubset(sv, args, 0, args->value);
    svDestroy(args);
}
