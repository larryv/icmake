#include "parser.ih"

int p_twoArgsFunTest(ExprType *type, SemVal const *larg, SemVal const *rarg)
{
    switch ((FunNr)*type)
    {
        case f_fgets:
        return svTestType(larg, e_str) && svTestType(rarg, e_list);


        case f_element:                     // f_element 
            if (svTestType(larg, e_int))    // first arg must be int 
            {                               // second arg == list: ok 
                if (svTestType(rarg, e_list))
                    return 1;
                if (svTestType(rarg, e_str))    // second arg == string: ok 
                {
                    *type = f_str_el;        // string element requested 
                    return 1;
                }
            }
        return 0;

        case f_resize:
        return svTestType(larg, e_str) && svTestType(rarg, e_int);

        case f_listfind:
        return svTestType(larg, e_list) && svTestType(rarg, e_str);

        case f_listunion:
        return svTestType(larg, e_list) && svTestType(rarg, e_str | e_list);

        default:
            //  case f_strchr:
            //  case f_strtok:
            //  case f_c_ext:
            //  case f_c_base:
            //  case f_c_path:
            //  case f_strfind:
        return larg->type & svType(rarg) & e_str;
    }
}
