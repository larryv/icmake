#include "parser.ih"

SemVal pmv_smaller(SemVal *lval, SemVal *rval)
{
    svBool2int(lval);                           // convert boolean to i 
    svBool2int(rval);

    if (p_conflict(lval, rval, op_eq))          // test type p_conflict 
        return pmv_semVal(2, lval, rval);

    if ((svType(lval) & svType(rval) & (unsigned)~e_typeMask) != e_const)
        *lval = pmv_binOp(lval, rval, op_sm);
    else if (svTestType(lval, e_int))
        svSetValue(lval, svValue(lval) < svValue(rval));
    else
    {
        svSetType(lval, e_int | e_const);
        svSetValue(lval, ssCompare(svValue(lval), svValue(rval)) < 0);
    }

    return SVmove(lval);
}
