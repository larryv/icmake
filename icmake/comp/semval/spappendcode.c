#include "semval.ih"

void sp_appendCode(SemVal *sv, int value, register unsigned size)
{
    register unsigned codelen;

    union
    {
        char buffer[2];
        int16_t int16;
    } u;

    msg("BEGIN");
    //svOut(sv);

    codelen = sv->code ?                    // use local codelen in register 
                sv->codelen
            :
                0;                          // 0 if not yet any code 

    if (size == sizeof(char))               // p_assign char to write 
        u.buffer[0] = (char)value;
    else                                    // use char[2] as intermediate 
        u.int16 = (int16_t)value;           // to store int16_t value    

                                            // make room for new code 
    sv->code = rss_realloc(sv->code, (codelen + size) * sizeof(char));

    msg("size = %u, buffer[0]: %x, [1]: %x", size, u.buffer[0], u.buffer[1]);
                                            // append the new code 
    memcpy(sv->code + codelen, u.buffer, size);

    sv->codelen = codelen + size;           // update the codelen-counter 

    msg("END");
    //svOut(sv);
}








