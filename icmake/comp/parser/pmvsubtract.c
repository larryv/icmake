#include "parser.ih"

SemVal pmv_subtract(SemVal *lval, SemVal *rval)
{
    register ExprType type;

    if (p_testBinOp(op_sub, lval, rval))
        return pmv_semVal(2, lval, rval);     // test for correct types 

    svBool2int(lval);                       // convert pending booleans 
    svBool2int(rval);

    if (p_conflict(lval, rval, op_sub))     // test type p_conflict 
        return pmv_semVal(2, lval, rval);     // test for correct types 

    type = svType(lval);                      // remember the type 

    if ((svType(lval) & svType(rval) & (unsigned)~e_typeMask) == e_const)
        svSetValue(lval, svValue(lval) - svValue(rval));
    else
    {
        *lval = pmv_binOp(lval, rval, op_sub);
        svSetType(lval, type & (e_typeMask | e_stack));
    }

    return SVmove(lval);
}
