//#define msgx

//  Possibilities:
//      1- pmv_makeList(int, string)
//      2- pmv_makeList(int, string, older, string)     -- younger ok too
//
//  The parser may insert the int-argument as int IS_FILE

//  makelist("*.c") is translated as:
//
//  String constants dump:
//      "*.c"
//  
//  Disassembled code:
//      [0014] 06 00 00   push string "*.c"
//      [0017] 05 01 00   push int 0001         // match-type (O_FILE, O_DIR)
//      [001a] 05 34 00   push int 0034         // not op_younger/op_older
//      [001d] 1b 1c      callrss 28 (makelist)
//      [001f] 1c 03      add sp, 3
//      [0021] 23         ret
//      [0022] 21 14 00   call [0014]
//      [0025] 04         push int 0
//      [0026] 24         pop reg
//      [0027] 1d         exit


#include "parser.ih"
                            // type op_hlt indicates not younger or older
                            // otherwise it's younger or older
                            // args: SemVal argument structure:
                            //       type of the list (FILE, DIR, ALL...)
                            //       regex for making the list
SemVal pmv_makeList(SemVal *args, ExprType type)
{
    if
    (                                       
        !svTestType(svArg(args, 0), e_int)  // first arg not int 
        ||                                  
        !svTestType(svArg(args, 1), e_str)  // or second not string 
        ||                      
            (                               
              svValue(args) == 3            // or three arguments, but 
              &&                            // last arg is not a string 
              !svTestType(svArg(args, 2), e_str)
            )
    )
    {
        util_semantic(sem_typeConflict, gp_funstring[f_makelist]);
        return SVtype(e_list);
    }

    SemVal tmp = SVint(type);               // define the decision type
    tmp = SVarg(&tmp);                      // tmp is now an argument struct.

    tmp = smvJoinArgs(&tmp, args);          // add the remaining arguments

    msg("initial #args %d", svValue(&tmp));
    
    msg("#args after adding decision type %x: %d", type, svValue(&tmp));

    svPushArgs(args, &tmp);                // args contains the pushed args

    msg("args pused into ret: code len: %d", svCodeLength(args));

    p_callRss(args, f_makelist, 3 + ((Opcode)type != op_hlt));

    msg("code length: %d", svCodeLength(args) );

    return SVmove(args);              // return makelist function call code
}





