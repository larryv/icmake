#include "parser.ih"

//  [0014] 06 04 00   push string "file"        // compare file
//  [0017] 06 00 00   push string "*.c"         // regex
//  [001a] 05 01 00   push int 0001             // match-mode
//  [001d] 05 18 00   push int 0018             // decision-mode
//  [0020] 1b 1c      callrss 28 (makelist)
//  [0022] 1c 04      add sp, 4


SemVal pmv_makeList2(ExprType decisionMode, SemVal *matchMode,
                   SemVal *regex, SemVal *compareFile)
{
    SemVal args = SVzero();
    args = smvAddArg(&args, matchMode);      // the match-mode (e.g., IS_FILE)
    args = smvAddArg(&args, regex);          // the regex
    args = smvAddArg(&args, compareFile);    // the compare file 

    return pmv_makeList(&args, decisionMode);
}
