//#define msgx

#include "semval.ih"

void svGrabFalse(unsigned **list, unsigned *len, SemVal *sv)
{
    *list = sv->falselist;
    sv->falselist = 0;

    *len = sv->falselen;
    sv->falselen = 0;
}
