//#define msgx

#include "parser.ih"

int p_tryString2IntConversion(SemVal *lval, SemVal *rval)
{
    msg("intypes: 0x%x, 0x%x", svType(lval), svType(rval));

    if                  // assigning a string of 1 char to an int is OK:   
    (                   // the int gets the ascii value of its character
        svTestType(lval, e_int)
        && 
        svTestType(rval, e_str | e_const)  == (e_str | e_const)
        && 
        ssLength(svValue(rval)) == 1
    )
    {
        svSetType(rval, e_int | e_const);           // change the str to int 
        svSetValue(rval, ssStr(svValue(rval))[0]);  // value is str's 1st
                                                    // character 
        return 1;
    }

    return 0;
}
