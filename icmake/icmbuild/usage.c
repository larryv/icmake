#include "icmbuild.ih"

int usage()
{
    rss_copyright("icmbuild");

    printf("Usage: icmbuild [option] args\n"
        "   option: only one option can be specified:\n"
        "       -c  - clears the screen ('tput clear') just before starting\n"
        "             the compilation process\n"
        "       -h  - shows this help-information (ends `icmbuild')\n"
        "             [also shown if `icmconf', required by `icmbuild',\n"
        "              does not exist in the current working directory]\n"
        "       -s  - install stripped compilation products at their\n"
        "             installation directories\n"
        "\n"
        "   args:\n"
        "       clean           - remove remnants of previous activities\n"
        "       cleangch        - remove all precompiled header files\n"
        "       cleantmp        - remove files created during compilation\n"
        "                         from TMP_DIR (precompiled headers stored\n"
        "                         under TMP_DIR are kept\n"
        "       [-s] install what where - install compilation products:\n"
        "               what:   - specify 'program', 'static', or 'shared'\n"
        "                         to install, resp. the program, the static "
                                                                "library,\n"
        "                         or the shared library\n"
        "               where:  - installation path (directory or "
                                                            "(optionally)\n"
        "                         a file when installing a program or a "
                                                            "static\n"
        "                         library). The path may start with ~/\n"
        "       [-c] library    - build a static library (requires "
                                                            "icmconf's\n"
        "                         LIBRARY specification);\n"
        "                         when SHARED is also specified in icmconf\n"
        "                         then a shared library is also built\n"
        "       [-c] program    - build a program (requires icmconf's MAIN\n"
        "                         specification)\n"
        "\n"
    );

    return 0;
}



