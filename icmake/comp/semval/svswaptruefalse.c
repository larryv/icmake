//#define msgx

#include "semval.ih"

void svSwapTrueFalse(SemVal *sv)
{
    unsigned len = sv->truelen;                   // cross the links 
    unsigned *list;

    sv->truelen = sv->falselen;
    sv->falselen = len;

    list = sv->truelist;
    sv->truelist = sv->falselist;
    sv->falselist = list;
}
