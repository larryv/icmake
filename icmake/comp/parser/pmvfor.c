#include "parser.ih"

SemVal pmv_for(SemVal *init, SemVal *cond, SemVal *inc, SemVal *stmnt)
{
    svPatchupContinue(stmnt, svCodeLength(stmnt));

    *stmnt = pmv_catStmnts(stmnt, inc);         // catenate inc to stmt 
    *cond = pmv_while(cond, stmnt, 0);          // create while-stmnt 

    symtab_pop();

    svCatenate(init, cond);                     // append the cond-code

    return SVmove(init);                        // return final code 
}
