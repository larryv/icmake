#include "parser.ih"

SemVal pmv_negate(SemVal *sv)                  
{
    if (p_testOperand(sv, op_umin))            // test types ok 
    {
        util_semantic(sem_illegalType, gp_opstring[op_umin]);
        return SVmove(sv);
    }

    if (svType(sv) & e_const)                  // immediate value 
        svSetValue(sv, -svValue(sv));
    else
    {
        svPushStack(sv);                            // convert to code 
        svAddCode(sv, op_umin);                // generate instruction 
    }

    return SVmove(sv);
}
