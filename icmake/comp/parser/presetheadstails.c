//#define msgx
#include "parser.ih"

void p_resetHeadsTails(SemVal *sv)
{
    msg("BEGIN");

    SemVal str = SVtype(e_str | e_const, "");   // empty string argument 

    svPushStack(&str);
    msg("push string code len: %d", svCodeLength(&str));

    svCatenate(sv, &str);                      // empty string on the stack 
    msg("svCatenate code len: %d", svCodeLength(sv));

    svAddCode(sv, op_call_rss, f_cmd_tail);    // head/tail arguments
    svAddCode(sv, op_call_rss, f_arg_tail);    // reset all cmd/arg
    svAddCode(sv, op_call_rss, f_arg_head);

            // callRss removes the pushed argument frm the stack. The previous
            // function calls share that argument, and must not pop the arg.
    p_callRss(sv, f_cmd_head);                  

    msg("END");
}
