#include "parser.ih"

SemVal pmv_continue()
{
    SemVal ret = SVzero();

    if (!gp_breakOK)
        util_semantic(sem_noContinue);
    else
    {                               
                                        // jump to the continue dest  
        svAddCode(&ret, op_jmp, j_continuelist);

        svSetType(&ret, e_bool | e_stack);

        deadInc();                    // next code is dead 
    }

    return ret;
}
