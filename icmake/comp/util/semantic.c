
#include "util.ih"

void util_semantic(SEMANTIC_ERR type, ...)
{
    va_list args;

    va_start(args, type);
                                // undefineds are always reported, other
                                // semantic errors at most ERRCOUNT times
                                // (per function definition)
    if (gu_semantic[type].count > 0 || type == sem_undefined)
    {
        rss_errorList(gu_sourceName, yylineno, gu_semantic[type].msg, args);
        --gu_semantic[type].count;
    }
}
