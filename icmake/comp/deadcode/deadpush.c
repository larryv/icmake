#include "deadcode.ih"

static unsigned size = 1;

void deadPush()
{
    if (++dp_idx >= size)               // too few elements ? then 5 more 
        dp_dead = rss_realloc(dp_dead, (size += 5) * sizeof(unsigned));

    dp_dead[dp_idx] = dp_dead[dp_idx - 1];  // copy former element 
}
