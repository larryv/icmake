#include "parser.ih"

SemVal pmv_listConst(SemVal *args)
{
    if (!p_allStrings(args))
        return SVzero();            // arguments not merely strings

    unsigned  nArgs = svValue(args);        // remember the #arguments

    SemVal codeArgs = smvArgs2code(args);   // convert the string args to code
    
    svPushArgs(args, &codeArgs);            // args pushed on the stack

    svAddCode(args, op_push_imm, nArgs);    // number of pushed arguments

    p_callRss(args, f_listConst, nArgs + 1);

    return SVmove(args);
}
