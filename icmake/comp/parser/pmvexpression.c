//#define msgx
#include "parser.ih"

SemVal pmv_expression(SemVal *e1)
{
    msg("BEGIN");
    //svOut(e1);

    svBool2int(e1);

    //rss_hexBytes("INIT b2i", e1->code, e1->codelen);

    if (svCopyVar())
        svPopVar(e1);
    else if
    (
        svTestType(e1, e_stack)
        &&
        !svTestType(e1, e_pre_inc_dec | e_post_inc_dec)
    )
        svAddCode(e1, op_asp, 1);

    //rss_hexBytes("INIT out", e1->code, e1->codelen);

    msg("END");
    return SVmove(e1);
}
